pref("extensions.dbus-integration.remoteWindowAccess", true);
pref("extensions.dbus-integration.remoteControlWindows", false);
pref("extensions.dbus-integration.remoteControlTabsAuto", false);
pref("extensions.dbus-integration.busName", "org.mozilla.firefox");
