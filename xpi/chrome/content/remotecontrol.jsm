
const EXPORTED_SYMBOLS = ['DBusRemoteControl'];

"use strict";

const Cc = Components.classes;
const Ci = Components.interfaces;
const Cu = Components.utils;
const CC = Components.Constructor;

const gThreadManager = Cc["@mozilla.org/thread-manager;1"].getService(Ci.nsIThreadManager);
const PrefService =	Cc["@mozilla.org/preferences-service;1"].getService(Ci.nsIPrefBranch);
const stringBundleService = Cc["@mozilla.org/intl/stringbundle;1"].getService(Ci.nsIStringBundleService);
const windowMediator = Cc["@mozilla.org/appshell/window-mediator;1"]
					.getService(Ci.nsIWindowMediator);
const Timer = CC("@mozilla.org/timer;1",
				"nsITimer");

Components.utils.import("resource://gre/modules/PrivateBrowsingUtils.jsm");
const DBusJSM = Cu.import("chrome://dbus-integration/content/dbus.jsm", {});
const EventPassingJSM = Cu.import("chrome://dbus-integration/content/evalByEventPassing.jsm");
const DBus = DBusJSM.DBus;
const evalByEventPassing = EventPassingJSM.evalByEventPassing;

const l10n = stringBundleService.createBundle("chrome://dbus-integration/locale/dbus-integration.properties");

// Interfaces
var WindowMediatorInterface = new DBus.Interface('org.mozilla.Firefox.WindowMediator');
WindowMediatorInterface.addMethod('getMostRecentWindow', '', 'o', null, ['object_path']);
WindowMediatorInterface.addMethod('getWindows', '', 'ao', null, ['object_paths']);
WindowMediatorInterface.addSignal('onOpenWindow', 'o', ['object_path']);

var WindowInterface = new DBus.Interface('org.mozilla.Firefox.Window');
WindowInterface.addProperty('name', 's', 'read');
WindowInterface.addProperty('privateBrowsing', 'b', 'read');
WindowInterface.addProperty('outerWidth', 'u', 'read');
WindowInterface.addProperty('outerHeight', 'u', 'read');
WindowInterface.addProperty('innerWidth', 'u', 'read');
WindowInterface.addProperty('innerHeight', 'u', 'read');
WindowInterface.addProperty('screenX', 'i', 'read');
WindowInterface.addProperty('screenY', 'i', 'read');
WindowInterface.addProperty('fullScreen', 'b', 'readwrite');
WindowInterface.addMethod('resizeTo', 'uu', '', ['width', 'height']);
WindowInterface.addMethod('resizeBy', 'ii', '', ['widthDif', 'heightDif']);
WindowInterface.addMethod('moveTo', 'ii', '', ['xPos', 'yPos']);
WindowInterface.addMethod('moveBy', 'ii', '', ['xPosDif', 'yPosDif']);
WindowInterface.addMethod('close');
WindowInterface.addMethod('focus');
WindowInterface.addSignal('onClose');
WindowInterface.addSignal('onFullScreen', 'b');
WindowInterface.annotations['org.freedesktop.DBus.Property.EmitsChangedSignal'] = false;

var TabBrowserInterface = new DBus.Interface('org.mozilla.Firefox.TabBrowser');
TabBrowserInterface.addMethod('getTabs', '', 'ao', null, ['object_paths']);
TabBrowserInterface.addMethod('getSelectedTab', '', 'o', null, ['object_path']);
TabBrowserInterface.addMethod('addTab', 's', 'o', ['url'], ['object_path']);
TabBrowserInterface.addMethod('addTabExt', 'sa{sv}', 'o', ['url', 'options']);
TabBrowserInterface.addSignal('onTabOpen', 'o', ['object_path']);
TabBrowserInterface.annotations['org.freedesktop.DBus.Property.EmitsChangedSignal'] = false;

var TabInterface = new DBus.Interface('org.mozilla.Firefox.Tab');
TabInterface.addSignal('onTabClose');
TabInterface.addSignal('onTabSelect');
TabInterface.addSignal('onTabPinned');
TabInterface.addSignal('onTabUnpinned');
TabInterface.addMethod('selectTab');
TabInterface.addProperty('label', 's', 'read');
TabInterface.addProperty('pinned', 'b', 'read');
TabInterface.annotations['org.freedesktop.DBus.Property.EmitsChangedSignal'] = false;

var BrowserInterface = new DBus.Interface('org.mozilla.Firefox.Browser');
BrowserInterface.addProperty('currentURI', 's', 'read');
BrowserInterface.addProperty('isLoading', 'b', 'read');
BrowserInterface.addProperty('contentTitle', 's', 'read');
BrowserInterface.addMethod('goBack');
BrowserInterface.addMethod('goForward');
BrowserInterface.addMethod('goHome');
BrowserInterface.addMethod('gotoIndex', 'i', null, ['history_index']);
BrowserInterface.addMethod('loadURI', 's', null, ['uri']);
BrowserInterface.addMethod('reload');
BrowserInterface.addMethod('stop');
BrowserInterface.addMethod('evalJavascript', 's', 'v', ['code'], ['result']);
BrowserInterface.addMethod('simulateClick', 'uuyb', '', ['x','y','button','double']);
BrowserInterface.addSignal('onLoad', 's', ['uri']);
BrowserInterface.annotations['org.freedesktop.DBus.Property.EmitsChangedSignal'] = false;

// Helpers
function confirm(browser, timeout) {
	let notificationBox = browser.ownerGlobal.getNotificationBox(browser.contentWindow);
	var result = null;
	// Build button descriptions
	let acceptButton = {
		label: l10n.GetStringFromName('notification.accept.text'),
		accessKey: l10n.GetStringFromName('notification.accept.accesskey'),
		popup: null,
		callback: function () { result = true; }
	}
	let declineButton = {
		label: l10n.GetStringFromName('notification.decline.text'),
		accessKey: l10n.GetStringFromName('notification.decline.accesskey'),
		popup: null,
		callback: function () { result = false; }
	}
	// Create notification
	let n = notificationBox.appendNotification(
		l10n.GetStringFromName('notification.text'), "dbus-remote-control",
		'chrome://dbus-integration/skin/dbus-16.png', notificationBox.PRIORITY_INFO_HIGH, [ acceptButton, declineButton ]);
	// Run timer
	let handle = browser.ownerGlobal.setTimeout(function() { result = false; }, timeout);
	// Wait until a result was decided
	while (result === null)
		gThreadManager.currentThread.processNextEvent(true);
	browser.ownerGlobal.clearTimeout(handle);
	notificationBox.removeNotification(n);
	
	return result;
}

function wrapMethod (object, name) {
	object[name] = function () { return this._call(name, arguments); }
}
function wrapProperty (object, name) {
	Object.defineProperty(object, name, {
		enumerable: true,
		configurable: true,
		get: function () { return this._get(name); },
		set: function (value) { return this._set(name, value); }
	});
}

// WindowMediator Wrappers
var WindowMediatorWrapper = {
	initialized: false,
	getMostRecentWindow : function () {
		let window = windowMediator.getMostRecentWindow("navigator:browser");
		var domWindowUtils = window.QueryInterface(Ci.nsIInterfaceRequestor)
			.getInterface(Ci.nsIDOMWindowUtils);
		let id = domWindowUtils.outerWindowID;
		return '/browserwindow'+id;
	},
	
	getWindows : function () {
		var result = [];
		if (!this.existingWindows) return result;
		for (let winid in this.existingWindows) {
			result.push(this.existingWindows[winid]);
		}
		return result;
	},
	
	Listener: {
		onOpenWindow: function (aWindow) {
			// Wait for the window to finish loading
			var domWindow = aWindow.QueryInterface(Ci.nsIInterfaceRequestor).getInterface(Ci.nsIDOMWindowInternal || Ci.nsIDOMWindow);
			var loadListener = function () {
				domWindow.removeEventListener("load", loadListener, false);
				WindowMediatorWrapper.updateWindowList();
				let domWindowUtils = domWindow.QueryInterface(Ci.nsIInterfaceRequestor).getInterface(Ci.nsIDOMWindowUtils);
			}
			domWindow.addEventListener("load", loadListener, false);
		},
		onCloseWindow: function (aWindow) {
			this.closeUpdateTimer = new Timer;
			this.closeUpdateTimer.target = gThreadManager.currentThread;
			this.closeUpdateTimer.initWithCallback(this, 10, Ci.nsITimer.TYPE_ONE_SHOT);
		},
		notify: function () {
			this.closeUpdateTimer = null;
			WindowMediatorWrapper.updateWindowList();
		},
		onWindowTitleChange: function (aWindow, aTitle) {},
		closeUpdateTimer: null,
	},
	
	init: function() {
		this.initialized = true;
		this.existingWindows = {};
		bus.addObjectInterface('/', WindowMediatorInterface, WindowMediatorWrapper);
		this.updateWindowList();
		windowMediator.addListener(WindowMediatorWrapper.Listener);
	},
	updateWindowList: function () {
		let windows = {};
		var enumerator = windowMediator.getEnumerator('navigator:browser');
		while(enumerator.hasMoreElements()) {
			// Get window
			let window = enumerator.getNext();
			let domWindowUtils = window.QueryInterface(Ci.nsIInterfaceRequestor)
				.getInterface(Ci.nsIDOMWindowUtils);
			// Get window id
			let id = domWindowUtils.outerWindowID;
			
			// Add it to the bus
			if (id in this.existingWindows) {
				windows[id] = this.existingWindows[id];
			}
			else {
				let path = '/browserwindow'+domWindowUtils.outerWindowID;
				let windowWrapper = new WindowWrapper(path, id);
				bus.addObjectInterface('/browserwindow'+domWindowUtils.outerWindowID, WindowInterface, windowWrapper);
				if (!PrivateBrowsingUtils.isWindowPrivate(window)) {
					let tabBrowserWrapper = new TabBrowserWrapper(path, id);
					bus.addObjectInterface('/browserwindow'+domWindowUtils.outerWindowID, TabBrowserInterface, tabBrowserWrapper);
				}
				windows[id] = path;
				this.onOpenWindow('/browserwindow'+domWindowUtils.outerWindowID);
			}
			// Remove no longer existing windows
			for (let winid in this.existingWindows) {
				if (!(winid in windows))
					bus.removeObject(this.existingWindows[winid]);
			}
			this.existingWindows = windows;
		}
	},
	destroy: function() {
		this.initialized = false;
		this.Listener.closeUpdateTimer = null;
		windowMediator.removeListener(WindowMediatorWrapper.Listener);
		
		for (let key in this.existingWindows) {
			let path = this.existingWindows[key];
			let winiface = bus.getObjectInterfaceImplementation(path, 'org.mozilla.Firefox.Window');
			let browseriface = bus.getObjectInterfaceImplementation(path, 'org.mozilla.Firefox.TabBrowser');
			bus.removeObject(path);
			if (browseriface)
				browseriface.destroy();
			if (winiface)
				winiface.destroy();
		}
		
		this.existingWindows = {};
		bus.removeObjectInterface('/', WindowMediatorInterface);
	}
}

// Browser Window Wrappers

function WindowWrapper(path, id) {
	this.id = id;
	this.path = path;
	for (let method in WindowInterface.methods) {
		wrapMethod(this, method);
	}
	for (let property in WindowInterface.properties) {
		wrapProperty(this, property);
	}
	
	Object.defineProperty(this, 'privateBrowsing', {
		enumerable: true,
		configurable: true,
		get: function () {
			let window = windowMediator.getOuterWindowWithId(this.id);
			return PrivateBrowsingUtils.isWindowPrivate(window);
		}
	});
	
	this._onunload = this._onunload.bind(this);
	this._onfullscreen = this._onfullscreen.bind(this);
	
	let window = windowMediator.getOuterWindowWithId(this.id);
	window.addEventListener('unload', this._onunload, false);
	window.addEventListener('fullscreen', this._onfullscreen, false);
}
WindowWrapper.prototype = {
	id: null,
	_checkAccessAllowed: function () {
		if (!PrefService.getBoolPref("extensions.dbus-integration.remoteControlWindows"))
			throw new Error('Access denied');
	},
	_call: function (name, args) {
		this._checkAccessAllowed();
		let window = windowMediator.getOuterWindowWithId(this.id);
		return window[name].apply(window, args);
	},
	_set: function (name, value) {
		this._checkAccessAllowed();
		let window = windowMediator.getOuterWindowWithId(this.id);
		window[name] = value;
	},
	_get: function (name) {
		let window = windowMediator.getOuterWindowWithId(this.id);
		return window[name];
	},
	_onunload: function(evt) {
		evt.target.removeEventListener("unload", this._onunload);
		this.onClose();
		this._onunload = null;
		this._onfullscreen = null;
	},
	_onfullscreen: function (evt) {
		this.onFullScreen(!evt.target.fullScreen);
	},
	destroy: function () {
		let window = windowMediator.getOuterWindowWithId(this.id);
		window.removeEventListener("unload", this._onunload);
		window.removeEventListener("fullscreen", this._onfullscreen);
		this._onunload = null;
		this._onfullscreen = null;
	}
};

function TabBrowserWrapper(path, id) {
	this.id = id;
	this.path = path;
	this.browser = windowMediator.getOuterWindowWithId(this.id).getBrowser();
	
	for (let method in TabBrowserInterface.methods) {
		if (!(method in this))
			wrapMethod(this, method);
	}
	for (let property in TabBrowserInterface.properties) {
		wrapProperty(this, property);
	}
	this.addTabExt = this.addTab;
	
	for (let i = 0; i < this.browser.tabs.length; ++i) {
		let tab = this.browser.tabs[i];
		this._registerTab(tab);
	}
	
	this._TabOpen = this._TabOpen.bind(this);
	this._TabClose = this._TabClose.bind(this);
	this._TabSelect = this._TabSelect.bind(this);
	
	this.browser.tabContainer.addEventListener('TabOpen', this._TabOpen, false);
	this.browser.tabContainer.addEventListener('TabClose', this._TabClose, false);
	this.browser.tabContainer.addEventListener('TabSelect', this._TabSelect, false);
}
TabBrowserWrapper.prototype = {
	id: null,
	_checkAccessAllowed: function () {
		if (!PrefService.getBoolPref("extensions.dbus-integration.remoteControlWindows"))
			throw new Error('Access denied');
	},
	_call: function (name, args) {
		if (name != 'addTab')
			this._checkAccessAllowed();
		var result = this.browser[name].apply(this.browser, args);
		if (result.linkedPanel && result.tagName && result.tagName == 'tab') {
			let data = result.linkedPanel.match(/panel-([^-]+)-([^-]+)/);
			return new DBus.ObjectPath('/browserwindow'+data[1]+'/tab'+data[2]);
		}
		return result;
	},
	_set: function (name, value) {
		this._checkAccessAllowed();
		this.browser[name] = value;
	},
	_get: function (name) {
		return this.browser[name];
	},
	_registerTab: function (tab) {
		let panelId = tab.linkedPanel;
		let path = this.path+'/tab'+panelId.replace(/^panel-[0-9]+-/, '');
		let tabWrapper = new TabWrapper(path, tab);
		bus.addObjectInterface(path, TabInterface, tabWrapper);
		if (tab.linkedBrowser) {
			let browserWrapper = new BrowserWrapper(path, tab.linkedBrowser);
			bus.addObjectInterface(path, BrowserInterface, browserWrapper);
		}
	},
	_TabSelect: function (evt) {
		let win = this.browser.ownerGlobal;
		win.document.getElementById('dbus-info-box').hidden=!(this.browser.selectedTab.linkedBrowser.dbusRemoteAllowed);
	},
	_TabOpen: function (evt) {
		this._registerTab(evt.target);
		let path = this.path+'/tab'+evt.target.linkedPanel.replace(/^panel-[0-9]+-/, '');
		this.onTabOpen(path);
	},
	_TabClose: function (evt) {
		let panelId = evt.target.linkedPanel;
		let path = this.path+'/tab'+panelId.replace(/^panel-[0-9]+-/, '');
		bus.removeObject(path);
	},
	getTabs: function() {
		var result = [];
		for (let i = 0; i < this.browser.tabs.length; ++i) {
			let tab = this.browser.tabs[i];
			let panelId = tab.linkedPanel;
			let path = this.path+'/tab'+panelId.replace(/^panel-[0-9]+-/, '');
			result.push(path);
		}
		return result;
	},
	getSelectedTab: function() {
		let tab = this.browser.selectedTab;
		let panelId = tab.linkedPanel;
		let path = this.path+'/tab'+panelId.replace(/^panel-[0-9]+-/, '');
		return path; 
	},
	destroy: function () {
		this.browser.tabContainer.removeEventListener('TabOpen', this._TabOpen);
		this.browser.tabContainer.removeEventListener('TabClose', this._TabClose);
		this.browser.tabContainer.removeEventListener('TabSelect', this._TabSelect);
		this.browser = null;
		this._TabOpen = null;
		this._TabClose = null;
	}
};

function TabWrapper(path, tab) {
	this.path = path;
	this.tab = tab;
	
	this._TabClose = this._TabClose.bind(this);
	this._onTabSelect = this._onTabSelect.bind(this);
	this._onTabPinned = this._onTabPinned.bind(this);
	this._onTabUnpinned = this._onTabUnpinned.bind(this);
	
	tab.addEventListener('TabClose', this._TabClose, false);
	tab.addEventListener('TabSelect', this._onTabSelect, false);
	tab.addEventListener('TabPinned', this._onTabPinned, false);
	tab.addEventListener('TabUnpinned', this._onTabUnpinned, false);
	
	for (let method in TabInterface.methods) {
		if (!(method in this))
			wrapMethod(this, method);
	}
	for (let property in TabInterface.properties) {
		wrapProperty(this, property);
	}
	
}
TabWrapper.prototype = {
	_checkAccessAllowed: function () {
		if (!PrefService.getBoolPref("extensions.dbus-integration.remoteControlWindows"))
			throw new Error('Access denied');
	},
	_TabClose: function () {
		this.onTabClose();
		this.destroy();
	},
	_call: function (name, args) {
		this._checkAccessAllowed();
		var result = this.tab[name].apply(this.tab, args);
		return result;
	},
	_set: function (name, value) {
		throw new Error('Access denied');
		this.tab[name] = value;
	},
	_get: function (name) {
		return this.tab[name];
	},
	selectTab: function () {
		this.tab.parentNode.tabbrowser.selectedTab = this.tab;
	},
	destroy: function () {
		this.tab.removeEventListener('TabClose', this._TabClose);
		this.tab.removeEventListener('TabSelect', this._onTabSelect);
		this.tab.removeEventListener('TabPinned', this._onTabPinned);
		this.tab.removeEventListener('TabUnpinned', this._onTabUnpinned);
		this.tab = null;
		this._TabClose = null;
		this._onTabSelect = null;
		this._onTabPinned = null;
		this._onTabUnpinned = null;
	},
	_onTabSelect: function () { this.onTabSelect() },
	_onTabPinned: function () { this.onTabPinned() },
	_onTabUnpinned: function () { this.onTabUnpinned() }
}

function BrowserWrapper (path, browser) {
	this.path = path;
	this.browser = browser;
	
	for (let method in BrowserInterface.methods) {
		if (!(method in this))
			wrapMethod(this, method);
	}
	for (let property in BrowserInterface.properties) {
		wrapProperty(this, property);
	}
	
	Object.defineProperty(this, 'currentURI', {
		enumerable: true,
		configurable: true,
		get: function () {
			return this.browser.currentURI.spec; 
		},
		set: function (value) {
			this.browser.currentURI.spec = value;
		}
	});
	
	Object.defineProperty(this, 'isLoading', {
		enumerable: true,
		configurable: true,
		get: function () {
			return this.browser.webProgress.isLoadingDocument; 
		},
	});
	this._onLoad = this._onLoad.bind(this);
	this.browser.addEventListener('load', this._onLoad, true);
}
BrowserWrapper.prototype = {
	_call: function (name, args) {
		this._checkAccessAllowed();
		var result = this.browser[name].apply(this.browser, args);
		return result;
	},
	_set: function (name, value) {
		this._checkAccessAllowed();
		this.browser[name] = value;
	},
	_get: function (name) {
		return this.browser[name];
	},
	_onLoad: function (evt) {
		this.onLoad(this.browser.currentURI.spec);
	},
	_checkAccessAllowed: function () {
		if (!this.browser.dbusRemoteAllowed) {
			if ((this.browser.dbusRemoteAllowed===undefined && PrefService.getBoolPref("extensions.dbus-integration.remoteControlTabsAuto")) || confirm(this.browser, 10000)) {
				if (this.browser === this.browser.ownerGlobal.gBrowser.selectedBrowser)
					this.browser.ownerGlobal.document.getElementById('dbus-info-box').hidden=false;
				this.browser.dbusRemoteAllowed = true;
			} else 
				throw new Error('Access denied');
		}
	},
	evalJavascript: function (code) {
		if (this.browser.securityUI.SSLStatus) throw new Error('Javascript injection not allowed on HTTPS');
		if (this.browser.contentDocument.getElementsByTagName('head').length == 0) throw new Error('Javascript injection not possible in non-HTML document');
		if (this.browser.contentDocument.getElementsByTagName('body').length == 0) throw new Error('Javascript injection not possible in non-HTML document');
		
		this._checkAccessAllowed();
		
		var waiting = true;
		var data = null;
		var timeoutTimer = new Timer;
		timeoutTimer.target = gThreadManager.currentThread;
		timeoutTimer.initWithCallback({
			notify: function() {
				waiting=false;
			}
		}, 5000, Ci.nsITimer.TYPE_ONE_SHOT);
		evalByEventPassing(this.browser.contentWindow, code, function (result) {
			data = result;
			waiting = false;
		});
		while (waiting)
			gThreadManager.currentThread.processNextEvent(true);
		if (!data)
			throw new Error('Timeout');
		if ('result' in data) {
			return data.result;
		}
		if (data.error) {
			var e = new Error(data.error);
			e.name = 'RemoteJavascriptException';
			throw e;
		}
		return {};
	},
	simulateClick: function(x, y, button, double) {
		this._checkAccessAllowed();
		
		var windowUtils = this.browser.contentWindow.QueryInterface(Components.interfaces.nsIInterfaceRequestor).getInterface(Components.interfaces.nsIDOMWindowUtils);
		windowUtils.sendMouseEventToWindow('mousedown', x, y, button, 1, 0, false);
		windowUtils.sendMouseEventToWindow('mouseup', x, y, button, 1, 0, false); 
		if (double) {
			windowUtils.sendMouseEventToWindow('mousedown', x, y, button, 2, 0, false);
			windowUtils.sendMouseEventToWindow('mouseup', x, y, button, 2, 0, false); 
		}
	},
	destroy: function () {
		this.browser.removeEventListener('load', this._onLoad);
		this.browser = null;
		this._onLoad = null;
	}
}

var bus = null;

function init() {
	if (!this.initialized) {
		// Connect to Session Bus
		bus = DBus.SessionBus();
		
		var busname = PrefService.getCharPref("extensions.dbus-integration.busName");
		this.setBusName(busname);
		
		var remoteWindowAccess = PrefService.getBoolPref("extensions.dbus-integration.remoteWindowAccess");
		this.setWindowExposure(remoteWindowAccess);
		
		PrefService.QueryInterface(Ci.nsIPrefBranch2);
		PrefService.addObserver("extensions.dbus-integration.busName", this, false);
		PrefService.addObserver("extensions.dbus-integration.remoteWindowAccess", this, false);
		PrefService.QueryInterface(Ci.nsIPrefBranch);
		this.initialized = true;
	}
}

function setBusName(busname) {
	bus.releaseAllNames();
	if (busname)
		bus.requestName(busname);
}

function setWindowExposure(bool) {
	// If wanted expose windows and tabs or destroy the wrapper
	if (bool) {
		if (!WindowMediatorWrapper.initialized)
			WindowMediatorWrapper.init();
	}
	else {
		if (WindowMediatorWrapper.initialized)
			WindowMediatorWrapper.destroy();
	}
}

function observe(subject, topic, data) {
	if (topic == "nsPref:changed") {
		if (data == "extensions.dbus-integration.busName") {
			this.setBusName(subject.getCharPref(data));
		}
		if (data == "extensions.dbus-integration.remoteWindowAccess") {
			this.setWindowExposure(subject.getBoolPref(data))
		}
	}
}

function deinit() {
	WindowMediatorWrapper.destroy();
	PrefService.QueryInterface(Ci.nsIPrefBranch2);
	PrefService.removeObserver("extensions.dbus-integration.busName", this);
	PrefService.removeObserver("extensions.dbus-integration.remoteWindowAccess", this);
	PrefService.QueryInterface(Ci.nsIPrefBranch);
	bus = null;
}

DBusRemoteControl = {
	initialized: false,
	init: init,
	setBusName: setBusName,
	setWindowExposure: setWindowExposure,
	destroy: deinit,
	observe: observe
}

