
"use strict";

const EXPORTED_SYMBOLS = [ 'createIntrospectXML' ];

const Cu = Components.utils;
Cu.import("resource://gre/modules/Services.jsm");

/*
function IntrospectXML(document) {
	if (document) {
		this.document = document;
	} else {
		var doctype = document.implementation.createDocumentType (
			"node",
			"-//freedesktop//DTD D-BUS Object Introspection 1.0//EN",
			"http://www.freedesktop.org/standards/dbus/1.0/introspect.dtd"
		);
		this.document = document = document.implementation.createDocument (null, 'node', doctype);
	}
	this.root = document.documentElement;
}
IntrospectXML.prototype = {
	document: null,
	addInterface: function () {
		let nIface = document.createElement('interface');
		this.root.appendChild(nIface);
		nIface.setAttribute('name', name);
	}
	
}
*/

function createItemXML(document, what, item) {
	let node = document.createElement(what);
	node.setAttribute('name', item.name);
	if (item.length) {
		for (let i = 0; i < item.length; ++i) {
			let arg = item[i];
			let nArg = document.createElement('arg');
			node.appendChild(nArg);
			nArg.setAttribute('type', arg.type);
			if (arg.name)
				nArg.setAttribute('name', arg.name);
			if (arg.direction)
				nArg.setAttribute('direction', arg.direction);
		}
	}
	if (item.type)
		node.setAttribute('type', item.type);
	if (item.access)
		node.setAttribute('access', item.access);
	for (let annotation in item.annotations) {
		let nAnnotation = document.createElement('annotation');
		node.appendChild(nAnnotation);
		nAnnotation.setAttribute('name', annotation);
		nAnnotation.setAttribute('value', item.annotations[annotation].toString());
	}
	return node;
}

function createIntrospectXML(interfaces, childNodes) {
	var doctype = Services.appShell.hiddenDOMWindow.document.implementation.createDocumentType (
		"node",
		"-//freedesktop//DTD D-BUS Object Introspection 1.0//EN",
		"http://www.freedesktop.org/standards/dbus/1.0/introspect.dtd"
	);
	var document = Services.appShell.hiddenDOMWindow.document.implementation.createDocument (null, "node", doctype);
	var root = document.documentElement;
	
	for (let i=0; i < interfaces.length; ++i) {
		let iface = interfaces[i];
		
		let nIface = document.createElement('interface');
		root.appendChild(nIface);
		nIface.setAttribute('name', iface.name);
		
		for (let method in iface.methods) {
			nIface.appendChild(createItemXML(document, 'method', iface.methods[method]));
		}
		for (let signal in iface.signals) {
			nIface.appendChild(createItemXML(document, 'signal', iface.signals[signal]));
		}
		for (let property in iface.properties) {
			nIface.appendChild(createItemXML(document, 'property', iface.properties[property]));
		}
		for (let annotation in iface.annotations) {
			let nAnnotation = document.createElement('annotation');
			root.appendChild(nAnnotation);
			nAnnotation.setAttribute('name', annotation);
			nAnnotation.setAttribute('value', iface.annotations[annotation].toString());
		}
	}
	
	for (let node in childNodes) {
		let nNode = document.createElement('node');
		nNode.setAttribute('name', node);
		root.appendChild(nNode);
	}
	
	return document;
}

