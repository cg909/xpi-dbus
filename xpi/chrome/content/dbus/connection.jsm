

const EXPORTED_SYMBOLS = ["Connection", "parseBusAddress"];

const Cc = Components.classes;
const Ci = Components.interfaces;
const Cr = Components.results;
const Cu = Components.utils;
const CC = Components.Constructor;

const gThreadManager = Cc["@mozilla.org/thread-manager;1"].getService(Ci.nsIThreadManager);
const socketTransportService = Cc["@mozilla.org/network/socket-transport-service;1"].getService(Ci.nsISocketTransportService);
const env = Cc["@mozilla.org/process/environment;1"].getService(Ci.nsIEnvironment);
const prng = Cc['@mozilla.org/security/random-generator;1'].getService(Ci.nsIRandomGenerator);
const ConsoleJSM = Cu.import("resource://gre/modules/devtools/Console.jsm", {});
const console = ConsoleJSM.console; 

const ScriptableInputStream = CC("@mozilla.org/scriptableinputstream;1",
								"nsIScriptableInputStream",
								"init");
const BinaryOutputStream = CC("@mozilla.org/binaryoutputstream;1",
							"nsIBinaryOutputStream",
							"setOutputStream");
const BinaryInputStream = CC("@mozilla.org/binaryinputstream;1",
						"nsIBinaryInputStream",
						"setInputStream");
const FileInputStream = CC("@mozilla.org/network/file-input-stream;1",
						 "nsIFileInputStream",
						 "init")
const FilePath = CC("@mozilla.org/file/local;1", "nsILocalFile", "initWithPath");
const CryptoHash = CC("@mozilla.org/security/hash;1", "nsICryptoHash", "initWithString");
const UnicodeConverter = CC("@mozilla.org/intl/scriptableunicodeconverter",
						  "nsIScriptableUnicodeConverter");
const XMLSerializer = CC("@mozilla.org/xmlextras/xmlserializer;1", "nsIDOMSerializer");
const Process = CC("@mozilla.org/process/util;1", "nsIProcess", "init");

Cu.import("resource://gre/modules/FileUtils.jsm");
Cu.import("chrome://dbus-integration/content/dbus/marshaling.jsm");
Cu.import("chrome://dbus-integration/content/dbus/interface.jsm");
Cu.import("chrome://dbus-integration/content/dbus/introspection.jsm");

/**
 * Split a DBus address into an object.
 * Used in connectSocket().
 * 
 * Example:
 * parseBusAddress('unix:abstract=/tmp/foo;guid=00000')
 * => { transport:'unix', abstract:'/tmp/foo', guid:'00000' }
 */
function parseBusAddress(busaddr) {
	var [transport, data] = busaddr.split(':', 2);
	var parts = data.split(',')
	var items = {}
	for (var i = 0; i < parts.length; ++i) {
		var [key, value] = parts[i].split('=', 2)
		items[decodeURIComponent(key)] = decodeURIComponent(value);
	}
	items.transport = transport;
	return items
}

function socatEscape(string) {
	return string.replace(/([\\\\,!'(){}\[\]])/g, '\\$1');
}

/**
 * @internal
 * Workaround for abstract namespace unix domain sockets.
 * Run ``socat`` to proxy the abstract socket to a non-abstract address.
 */
function createAbstractSocketHelper(addr) {
	if (addr.transport != "unix") throw "Not an unix socket address";
	if (addr.abstract === undefined) throw "Not an abstract socket address";
	
	let newpath = FileUtils.getFile("ProfD", [FilePath(addr.abstract).leafName+'.sock']);
	
	let program = FilePath('/usr/bin/socat');
	
	if (!program.exists()) {
		console.info("'socat' not found but needed for DBus connection to ", addr);
		throw "'socat' not found";
	}
	
	let process = Process(program);
	let args = ['UNIX-LISTEN:'+socatEscape(newpath.path)+',unlink-early,unlink-close,mode=600', 'ABSTRACT-CONNECT:'+socatEscape(addr.abstract)];
	console.info("Running socat to proxy abstract unix domain socket", program.path, args);
	process.run(false, args, args.length);
	
	// HACKY, but it works.. wait for the socket
	for (let i = 0; i < 2000000; ++i) {
		gThreadManager.currentThread.processNextEvent(false);
		if (newpath.exists()) break;
	}
	if (!newpath.exists())
		throw "Creating socat proxy for DBus failed!"
	
	return newpath.path;
}

/**
 * @internal
 * Create a nsISocketTransport instance from a DBus address string.
 */
function connectSocket(busaddr) {
	var addr = parseBusAddress(busaddr)
	if (addr.transport == 'unix') {
		// Check key-value pairs and extract unix path
		var path = null;
		if (addr.path !== undefined)
			path = addr.path;
		else if (addr.abstract !== undefined)
			path = createAbstractSocketHelper(addr);
		else
			throw "Invalid DBus address";
		
		let file = FilePath(path);
		console.debug('DBus: connecting to unix socket', file.path)
		
		// Create connection
		return socketTransportService.createUnixDomainTransport(file);
	}
	else if (addr.transport == 'tcp' || addr.transport == 'nonce-tcp') {
		if (addr.host === undefined || addr.port === undefined)
			throw "Invalid DBus address";
		if (addr.transport == 'nonce-tcp' && addr.noncefile === undefined)
			throw "Invalid DBus address";
		// Create connection
		console.debug('DBus: connecting to tcp socket "%s"', file.path)
		var socket = socketTransportService.createTransport(null, 0, addr.host, addr.port, null);
		// Modify flags
		if (addr.family) {
			if (addr.family == 'ipv4') 
				socket.connectionFlags |= socket.DISABLE_IPV6;
			else if (addr.family == 'ipv6')
				socket.connectionFlags |= socket.DISABLE_IPV4;
		}
		if (addr.noncefile) {
			throw "Nonce file support unimplemented!";
		}
		return socket;
		
	}
	else
		throw "Invalid DBus address or unsupported transport: '"+encodeURIComponent(addr.transport)+"'";
}

/**
 * @internal
 * Hex-encode a string.
 */
function encodeHexString(string) {
	let result = [];
	for (let i = 0; i < string.length; ++i) {
		let hex = string.charCodeAt(i).toString(16);
		if (hex.length == 0) hex = '00';
		if (hex.length == 1) hex = '0'+hex;
		result.push(hex);
	}
	return result.join('');
}

/**
 * @internal
 * Hex-encode a bytearray.
 */
function encodeHexBytes(bytes) {
	let result = [];
	for (let i = 0; i < bytes.length; ++i) {
		let hex = bytes[i].toString(16);
		if (hex.length == 0) hex = '00';
		if (hex.length == 1) hex = '0'+hex;
		result.push(hex);
	}
	return result.join('');
}

/**
 * @internal
 * Hex-decode a string.
 */
function decodeHexString(string) {
	result = [];
	for (let i = 0; i < string.length; i+=2) {
		result.push(Number('0x'+string.substr(i, 2)));
	}
	return String.fromCharCode.apply(null, result);
}

/**
 * @internal
 * Generate a strings hex-encoded SHA1 hash
 */
function sha1String(string) {
	let result = {};
	let converter = new UnicodeConverter();
	converter.charset = 'ASCII';
	let data = converter.convertToByteArray(string, result);
	let sha1 = new CryptoHash('SHA1');
	sha1.update(data, data.length);
	return encodeHexString(sha1.finish(false));
	
}

/**
 * @internal
 * Handler for the DBus Authentication Protocol
 */
function ConnectionAuthHandler(asyncInput, output) {
	this.buffer = "";
	this.state = 101;
	this.asyncInput = asyncInput;
	this.output = output;
}
ConnectionAuthHandler.prototype = {
	state: null,
	guid: null,
	authMethod: null,
	
	supportedAuthMethods: [
		'DBUS_COOKIE_SHA1',
		'ANONYMOUS'
	],
	
	onAuthList: function(answer, answerdata) {
		if (answer == 'REJECTED') {
			this.peerAuthMethods = answerdata.split(' ');
			return this.tryNextAuthMethod()
		}
		else
			this.state = 'error';
	},
	
	sendError: function(string) {
		if (!string)
			this.output.write("ERROR\r\n", 7);
		else
			this.output.write("ERROR \""+string+"\"\r\n", 10+string.length);
	},
	
	tryNextAuthMethod: function() {
		let list = this.peerAuthMethods;
		while (this.authMethods.length > 0) {
			let method = this.authMethods.shift()
			for (let i = 0; i < list.length; ++i) {
				if (list[i] == method) {
					// Supported method found
					return this['doAuth'+list[i]].call(this);
				}
			}
		}
		this.state = 'error';
	},
	
	doAuthANONYMOUS: function() {
		this.currentHandler = function (answer, answerdata) {
			if (answer != 'OK') {
				this.tryNextAuthMethod();
			}
			else {
				this.guid = answerdata;
				this.output.write("BEGIN\r\n", 7);
				this.state = 'done';
			}
		}
		this.output.writeBytes("AUTH ANONYMOUS\r\n", 16);
	},
	
	doAuthDBUS_COOKIE_SHA1: function() {
		this.currentHandler = function (answer, answerdata) {
			if (answer == 'OK') {
				this.guid = answerdata;
				this.output.write("BEGIN\r\n", 7);
				this.state = 'done'
			}
			else if (answer == 'DATA') {
				let [ context, cookieid, challenge ] = decodeHexString(answerdata).split(' ');
				// Build path to cookie directory
				let file = FilePath(env.get('HOME'));
				file.append('.dbus-keyrings');
				// Check directory permissions
				if (!file.exists() || (!file.isDirectory()) || (file.permissions & 0x3f /* = 0077*/ ) != 0) {
					this.sendError('.dbus-keyrings does not exist or has wrong permissions');
					return;
				}
				// Open cookie file
				file.append(context);
				if (!file.exists() || !file.isFile() || !file.isReadable()) {
					this.sendError('Cookie file not readable');
					return;
				}
				let istream = new FileInputStream(file, 0x01, 0600, 0);
				istream.QueryInterface(Components.interfaces.nsILineInputStream);
				
				// Read cookie
				let line = {}, hasmore, found=false;
				do {
					hasmore = istream.readLine(line);
					var [item_id, item_timestamp, item_cookie] = line.value.split(' ', 3);
					if (item_id == cookieid) {
						found = true;
						break;
					}
				} while(hasmore);
				istream.close();
				if (!found) {
					this.sendError('Cookie not found');
					return;
				}
				// Generate challenge answer
				let myChallenge = encodeHexBytes(prng.generateRandomBytes(32));
				let data = challenge+':'+myChallenge+':'+item_cookie;
				let sha1string = sha1String(data);
				let myAnswer = encodeHexString(myChallenge+" "+sha1string);
				// Send answer
				this.output.writeBytes("DATA "+myAnswer+"\r\n", 7+myAnswer.length);
			}
			else {
				if (answer == 'ERROR')
					this.sendError();
				this.tryNextAuthMethod();
			}
		}
		let username = env.get('USER');
		let encoded_username = encodeHexString(username);
		this.output.writeBytes("AUTH DBUS_COOKIE_SHA1 "+encoded_username+"\r\n", 24+encoded_username.length);
	},
	
	
	blockingConnect: function() {
		// Init attributes
		this.authMethods = this.supportedAuthMethods.slice();
		this.peerAuthMethods = [];
		this.currentHandler = null;
		this.buffer = "";
		// Set async input handler
		this.asyncInput.asyncWait(this, 0, 0, gThreadManager.currentThread);
		this.currentHandler = this.onAuthList;
		this.state = 'running';
		// Send first command
		this.output.writeBytes("\0", 1);
		this.output.writeBytes("AUTH\r\n", 6);
		while (this.state == 'running') {
			gThreadManager.currentThread.processNextEvent(true);
		}
		return (this.state == 'done');
	},
	
	handleAnswer: function(answer) {
		var cmd, data;
		var i = answer.indexOf(' ');
		if (i < 0) {
			cmd = answer;
			data = '';
		}
		else {
			cmd = answer.substr(0,i);
			data = answer.substr(i+1);
		}
		this.currentHandler.call(this, cmd, data)
	},
	
	onInputStreamReady: function(input)
	{
		try
		{
			input = new ScriptableInputStream(input);
			var data = input.read(input.available());
			
			if (data == '') {
				this.asyncInput.close();
				this.output.close();
				this.state = 101;
				return;
			} else {
				var i = data.indexOf('\n');
				if (i == -1) {
					this.buffer += data;
					data = '';
				} else {
					data = this.buffer+data.substr(0,i+1)
					this.buffer = data.substr(i+1)
				}
				
				if (data) {
					this.handleAnswer(data.substr(0,data.length-2));
				}
			}
		}
		catch (e)
		{
			console.error("Exception on read from DBus socket: ",e);
			this.asyncInput.close();
			this.output.close();
			this.state = this.STATE_ERROR;
			return;
		}
		if (this.state != 'done' && this.state != 'error')
			this.asyncInput.asyncWait(this, 0, 0, gThreadManager.currentThread);
	},
	

	QueryInterface: function(aIID)
	{
		if (aIID.equals(Ci.nsIInputStreamCallback) || aIID.equals(Ci.nsISupports))
			return this;

		throw Cr.NS_ERROR_NO_INTERFACE;
	},
}

var generic_org_freedesktop_DBus_Peer_Handler = {
	Ping: function () { return; },
	GetMachineId: function () {
		throw "GetMachineId currently not implemented";
	}
}

function IntrospectableHandler(node) {
	this.node = node;
}
IntrospectableHandler.prototype = {
	Introspect: function () {
		let handlers = this.node.handlers;
		let children = this.node.children;
		let interfaces = [ handlers[key].interface for (key in handlers) if (handlers[key].invisible !== true) ];
		var xml = createIntrospectXML(interfaces, children);
		return (new XMLSerializer()).serializeToString(xml);
	}
}

function PropertiesHandler(node) {
	this.node = node;
}
PropertiesHandler.prototype = {
	Get: function (interface_name, property_name) {
		let handlers = this.node.handlers;
		let handler = this.node.handlers[interface_name];
		if (!handler) {
			throw "Interface not found";
		}
		let property = handler.interface.properties[property_name];
		if (!property) {
			throw "Property not found";
		}
		if (property.access == 'write') {
			throw "Property not readable";
		}
		return handler.object[property_name];
	},
	Set: function (interface_name, property_name, value) {
		let handlers = this.node.handlers;
		let handler = this.node.handlers[interface_name];
		if (!handler) {
			throw "Interface not found";
		}
		let property = handler.interface.properties[property_name];
		if (!property) {
			throw "Property not found";
		}
		if (property.access != 'write' && property.access != 'readwrite') {
			throw "Property not writeable";
		}
		handler.object[property_name] = value;
	},
	GetAll: function (interface_name) {
		let result = {};
		let handlers = this.node.handlers;
		let handler = this.node.handlers[interface_name];
		if (!handler) {
			throw "Interface not found";
		}
		for (let property_name in handler.interface.properties) {
			let property = handler.interface.properties[property_name];
			if (property.access == 'write') {
				continue;
			}
			result[property_name] = handler.object[property_name];
		}
		return result;
	}
}

/**
 * DBus Connection Class
 */
function Connection(addr) {
	this._addr = addr;
	this._serial = 1;
	this._callbacks = {};
	this._signalHandlers = {};
	this._objectTree = { children: {}, handlers: {}};
	
	console.debug('DBus: creating connection to ', addr);
	
	this._objectTree.handlers['org.freedesktop.DBus.Peer'] = { interface: StandardInterfaces.Peer, object: generic_org_freedesktop_DBus_Peer_Handler, invisible:true };
	this._objectTree.handlers['org.freedesktop.DBus.Introspectable'] = { interface: StandardInterfaces.Introspectable, object: new IntrospectableHandler(this._objectTree) };
	
	let transport = this._transport = connectSocket(addr);
	console.debug('DBus: opening streams')
	rawOutput = transport.openOutputStream(transport.OPEN_BLOCKING, 0, 0);
	this._output = new BinaryOutputStream(rawOutput);
	this._asyncInput = transport.openInputStream(0, 0, 0).QueryInterface(Ci.nsIAsyncInputStream);
	
	console.debug('DBus: authenticating')
	let initializer = new ConnectionAuthHandler(this._asyncInput, this._output);
	if (!initializer.blockingConnect())
		throw "DBus Connection to '"+encodeURI(addr)+"' failed!";
	console.debug('DBus: connection successful')
	initializer = null;
	this._buffer = [];
	this._asyncInput.asyncWait(this, 0, 0, gThreadManager.currentThread);

}
Connection.prototype = {
	
	close: function () {
		this._callbacks = {};
		this._signalHandlers = {};
		if (this._asyncInput)
			this._asyncInput.closeWithStatus(Cr.NS_BASE_STREAM_CLOSED);
		this._asyncInput = null;
		if (this._output)
			this._output.close();
		this._output = null;
		if (this._transport)
			this._transport.close(0);
		this._transport = null;
	},
	
	emitSignal: function (path, interface_, name, signature, arguments, destination) {
		// Build flags
		let flags = 1|2;
		// Build header fields
		let fields = [[1, new ObjectPath(path)], [2, interface_], [3, name]];
		if (destination)
			fields.push([6, destination]);
		if (signature && signature.length > 0)
			fields.push([8, new Signature(signature)]);
		// Marshal arguments
		var body = null;
		if (signature && signature.length > 0) {
			body = marshal(arguments, signature);
		}
		// Send message
		this._sendMessage(4, flags, fields, body);
	},
	
	callMethod: function (path, method, signature, arguments, destination, interface_, callback, errorCallback, noAutoStart) {
		// Build flags
		let flags = 0;
		if (!callback && !errorCallback) flags |= 1; // NO_REPLY_EXPECTED
		if (noAutoStart) flags |= 2; // NO_AUTO_START
		// Build header fields
		let fields = [[1, new ObjectPath(path)]];
		if (destination)
			fields.push([6, destination]);
		if (interface_)
			fields.push([2, interface_]);
		fields.push([3, method]);
		if (signature && signature.length > 0)
			fields.push([8, new Signature(signature)]);
		// Set callback
		var serial = this._serial;
		if (callback || errorCallback) {
			this._callbacks[serial] = {
				destination: destination,
				callback: callback,
				errorCallback: errorCallback,
				thread: gThreadManager.currentThread
			};
		}
		// Marshal arguments
		var body = null;
		if (signature && signature.length > 0) {
			body = marshal(arguments, signature);
		}
		// Send message
		try {
			this._sendMessage(1, flags, fields, body);
		} catch (e) {
			if (this._callbacks[serial])
				delete this._callbacks[serial];
			throw e;
		}
	},
	
	_sendMethodReturn: function (reply_serial, signature, arguments, destination) {
		// Build flags
		let flags = 1|2; // NO_REPLY_EXPECTED | NO_AUTO_START
		// Build header fields
		let fields = [[5, reply_serial]];
		if (destination)
			fields.push([6, destination]);
		if (signature && signature.length > 0)
			fields.push([8, new Signature(signature)]);
		// Marshal arguments
		let body = null;
		if (signature && signature.length > 0) {
			body = marshal(arguments, signature);
		}
		// Send message
		this._sendMessage(2, flags, fields, body);
	},
	
	_sendError: function (reply_serial, error_name, signature, arguments, destination) {
		// Build flags
		let flags = 1|2; // NO_REPLY_EXPECTED | NO_AUTO_START
		// Build header fields
		let fields = [[5, reply_serial], [4, error_name]];
		if (destination)
			fields.push([6, destination]);
		if (signature && signature.length > 0)
			fields.push([8, new Signature(signature)]);
		// Marshal arguments
		let body = null;
		if (signature && signature.length > 0) {
			body = marshal(arguments, signature);
		}
		// Send message
		this._sendMessage(3, flags, fields, body);
	},
	
	toString: function () {
		return '[dbus.Connection "'+encodeURI(this._addr)+'"'+(this._transport===null?' (closed)':'')+']';
	},
	
	_sendMessage: function (typecode, flags, headerfields, body) {
		try {
			var header = marshal([ (machineBigEndian?'B':'l'), typecode, flags, 1, (body?body.length:0), this._serial, headerfields ], 'yyyyuua(yv)', 0, 8);
			var messageArray = Array.prototype.slice.call(header, 0);
			if (body) {
				body = Array.prototype.slice.call(body, 0);
				for (let i = 0; i < body.length; ++i) {
					messageArray.push(body[i]);
				}
			}
			this._output.writeByteArray(messageArray, messageArray.length);
			return this._serial;
		} finally {
			this._serial++;
		}
	},
	
	_handleMessage: function (header, body) {
		if (header[1] == 0) console.error("DBus: received invalid message: ", header, body);
		let serial = header[5];
		let flags = header[2];
		let destination = header[6][6];
		let sender = header[6][7];
		if (header[1] == 1) { 
			// METHOD_CALL
			let path = header[6][1];
			let member = header[6][3];
			let interface_ = header[6][2];
			this._handleMethodCall(sender, destination, path, member, interface_, body, serial, flags);
		}
		else if (header[1] == 2) { 
			// METHOD_RETURN
			let reply_serial = header[6][5];
			this._handleMethodReturn(sender, destination, reply_serial, body, serial, flags);
		}
		else if (header[1] == 3) { 
			// ERROR_REPLY
			let reply_serial = header[6][5];
			this._handleErrorReply(sender, destination, reply_serial, body, serial, flags);
		}
		else if (header[1] == 4) { 
			// SIGNAL
			let path = header[6][1];
			let member = header[6][3];
			let interface_ = header[6][2];
			this._handleSignal(sender, destination, path, member, interface_, body, serial, flags);
		}
	},
	
	_handleMethodReturn: function (sender, destination, reply_serial, body, serial, flags) {
		let callbackData = this._callbacks[reply_serial];
		if (!callbackData) {
			return;
		}
		if (callbackData.callback) {
			callbackData.callback.apply(null, body);
		}
		
	},
	
	_handleErrorReply: function (sender, destination, reply_serial, body, serial, flags) {
		let callbackData = this._callbacks[reply_serial];
		if (!callbackData) {
			return;
		}
		if (callbackData.errorCallback) {
			callbackData.errorCallback.apply(null, body);
		}
	},
	
	_handleSignal: function (sender, destination, path, member, interface_, body, serial, flags) {
		let signalHandlers = this._signalHandlers[interface_+'.'+member];
		if (!signalHandlers) return;
		for (let i = 0; i < signalHandlers.length; ++i) {
			let handler = signalHandlers[i];
			if (handler.path != null && handler.path != path)
				continue;
			if (handler.sender != null && handler.sender != sender)
				continue;
			try {
				handler.callback.apply(null, body);
			} catch (e) {
				console.error("DBus: Exception in signal handler ", signalHandlers[i], e);
			}
		}
	},
	
	_handleMethodCall: function (sender, destination, path, member, interface_, body, serial, flags) {
		let methodHandlers = null;
		if (!sender) sender = null;
		
		// Locate object in tree
		let pathComponents = path.split('/');
		pathComponents.shift();
		let currentObject = this._objectTree;
		while (pathComponents.length > 0) {
			let component = pathComponents.shift();
			if (component == '') continue;
			currentObject = currentObject.children[component];
			if (!currentObject) break;
		}
		
		if (!currentObject) {
			if ((flags & 1) == 0)
				this._sendError(serial, 'org.freedesktop.DBus.Error.UnknownObject', 's', ["No such object path '"+encodeURI(path)+"'"], sender);
			return;
		}
		
		// Locate interface implementation
		let handler = null;
		if (interface_) {
			handler = currentObject.handlers[interface_];
		}
		else {
			for (let key in currentObject.handlers) {
				if (member in currentObject.handlers[key].interface.methods) { 
					handler = currentObject.handlers[key];
					break;
				}
			}
		}
		// Return error if no handler found
		if (!handler) {
			if ((flags & 1) == 0) {
				if (interface_)
					this._sendError(serial, 'org.freedesktop.DBus.Error.UnknownInterface', 's', ["No such method '"+encodeURI(interface_)+'.'+encodeURI(member)+"' at object path '"+encodeURI(path)+"'"], sender);
				else
					this._sendError(serial, 'org.freedesktop.DBus.Error.UnknownMethod', 's', ["No such method '"+encodeURI(member)+"' in any interface at object path '"+encodeURI(path)+"'"], sender);
			}
			return;
		}
		// Return error if method not in interface
		if (!handler.interface.methods[member]) {
			if ((flags & 1) == 0)
				this._sendError(serial, 'org.freedesktop.DBus.Error.UnknownMethod', 's', ["No such method '"+encodeURI(interface_)+'.'+encodeURI(member)+"' at object path '"+encodeURI(path)+"'"], sender);
			return;
		}
		// Return error if method not found
		if (!handler.object[member] || !handler.object[member].apply) {
			if ((flags & 1) == 0)
				this._sendError(serial, 'org.freedesktop.DBus.Error.UnknownMethod', 's', ["Unimplemented method '"+encodeURI(handler.interface.name)+'.'+encodeURI(member)+"' at object path '"+encodeURI(path)+"'"], sender);
			return;
		}
		
		// Call 
		try {
			let result = handler.object[member].apply(handler.object, body);
			if ((flags & 1) == 0) {
				let signature = handler.interface.methods[member].out_signature;
				let returnvalues = [];
				if (signature.length > 1 && splitSignature(signature).length > 1) {
					returnvalues = result;
				}
				else if (signature.length > 0) {
					returnvalues = [result];
				}
				this._sendMethodReturn(serial, signature, returnvalues, sender);
			}
			return;
		} catch (e) {
			console.log('Exception in method called by DBus', e);
			if ((flags & 1) == 0) {
				if (typeof e == 'object' && e.message && e.name) {
					/*let edict = {name: e.name};
					for (let key in e) {
						let value = e[key];
						if (typeof value == 'number' || typeof value == 'string' || typeof value == 'boolean' ||
							value instanceof Number || value instanceof String || value instanceof Boolean)
							edict[key] = value;
					}*/
					this._sendError(serial, 'org.mozilla.Firefox.Javascript.'+e.name, 's', [e.message], sender);
				} else 
					this._sendError(serial, 'org.mozilla.Firefox.Javascript.Error', 's', [e.toString()], sender);
			}
		}
	},
	
	_addObjectSignalHandler: function (path, interface_, signal_name, object) {
		var bus = this;
		var signal = interface_.signals[signal_name];
		object[signal_name] = function () {
			bus.emitSignal(path, interface_.name, signal_name, signal.signature, arguments);
		}
	},
	
	addObjectInterface: function (path, interface_, object) {
		// Locate and create object in tree
		let pathComponents = path.split('/');
		pathComponents.shift();
		let currentNode = this._objectTree;
		while (pathComponents.length > 0) {
			let component = pathComponents.shift();
			if (component == '') continue;
			if (!(component in currentNode.children)) {
				let newNode = { children: {}, handlers: {}};
				newNode.handlers['org.freedesktop.DBus.Peer'] = { interface: StandardInterfaces.Peer, object: generic_org_freedesktop_DBus_Peer_Handler, invisible:true };
				newNode.handlers['org.freedesktop.DBus.Introspectable'] = { interface: StandardInterfaces.Introspectable, object: new IntrospectableHandler(newNode) };
				currentNode.children[component] = newNode;
			}
			currentNode = currentNode.children[component];
		}
		if (interface_) {
			currentNode.handlers[interface_.name] = { interface: interface_, object: object };
			if (interface_.hasProperties) {
				if (!('org.freedesktop.DBus.Properties' in currentNode.handlers)) {
					currentNode.handlers['org.freedesktop.DBus.Properties'] = { interface: StandardInterfaces.Properties, object: new PropertiesHandler(currentNode) };
				}
			}
			
			for (let signal_name in interface_.signals) {
				this._addObjectSignalHandler(path, interface_, signal_name, object);
			}
		}
	},
	
	objectExists: function (path) {
		// Locate object in tree
		let pathComponents = path.split('/');
		pathComponents.shift();
		let currentNode = this._objectTree;
		while (pathComponents.length > 0) {
			let component = pathComponents.shift();
			if (component == '') continue;
			if (!(component in currentNode.children)) {
				return false;
			}
			currentNode = currentNode.children[component];
		}
		if (currentNode) return true;
	},
	
	getObjectInterfaceImplementation: function (path, iface_name) {
		if (iface_name.name)
			iface_name = iface_name.name;
		// Locate object in tree
		let pathComponents = path.split('/');
		pathComponents.shift();
		let parentNode = null;
		let currentNode = this._objectTree;
		let leafComponent = '';
		while (pathComponents.length > 0) {
			let component = pathComponents.shift();
			if (component == '') continue;
			if (!(component in currentNode.children)) {
				return null;
			}
			parentNode = currentNode;
			currentNode = currentNode.children[component];
			leafComponent = component;
		}
		
		return currentNode.handlers[iface_name].object;
	},
	
	removeObjectInterface: function (path, iface_name) {
		if (iface_name.name)
			iface_name = iface_name.name;
		// Locate object in tree
		let pathComponents = path.split('/');
		pathComponents.shift();
		let parentNode = null;
		let currentNode = this._objectTree;
		let leafComponent = '';
		while (pathComponents.length > 0) {
			let component = pathComponents.shift();
			if (component == '') continue;
			if (!(component in currentNode.children)) {
				return false;
			}
			parentNode = currentNode;
			currentNode = currentNode.children[component];
			leafComponent = component;
		}
		
		delete currentNode.handlers[iface_name];
	},
	
	removeObject: function (path) {
		// Locate object in tree
		let pathComponents = path.split('/');
		pathComponents.shift();
		let parentNode = null;
		let currentNode = this._objectTree;
		let leafComponent = '';
		while (pathComponents.length > 0) {
			let component = pathComponents.shift();
			if (component == '') continue;
			if (!(component in currentNode.children)) {
				return false;
			}
			parentNode = currentNode;
			currentNode = currentNode.children[component];
			leafComponent = component;
		}
		
		if (parentNode) {
			delete parentNode.children[leafComponent];
		}
		
		for (let iface in currentNode.handlers) {
			if (iface == 'org.freedesktop.DBus.Peer')
				continue
			if (parentNode === null && iface == 'org.freedesktop.DBus.Introspectable')
				continue
			delete currentNode.handlers[iface];
		}
		currentNode.children = {};
	},
	
	// Implement nsIInputStreamCallback:
	onInputStreamReady: function(input)
	{
		if (!this._transport) return;
		try
		{
			input = new BinaryInputStream(input);
			let bytesWanted = input.available();
			if (bytesWanted === 0) {
				this.close();
				throw Cr.NS_BASE_STREAM_CLOSED;
			}
			var data = input.readByteArray(bytesWanted);
			for (let i = 0; i < data.length; ++i)
				this._buffer.push(data[i]);
		}
		catch (e)
		{
			console.error("Exception on read from DBus socket:",e);
			this._out = null;
			this.close();
			return;
		}
		
		
		while (this._buffer.length >= 16) {
			// Check BOM or flush buffer if invalid
			let BOM = this._buffer[0];
			let bigEndian;
			if (BOM == 66)
				bigEndian = true;
			else if (BOM == 108)
				bigEndian = false;
			else {
				this._buffer = [];
				break;
			}
			// Decode header
			let header, headersize;
			try {
				[header, headersize] = unmarshal('yyyyuua{yv}', this._buffer, bigEndian, 0);
			}
			catch (e) {
				// On decoding error flush the buffer
				this._buffer = [];
				throw e;
			}
			// Apply header padding
			if (headersize % 8 != 0) headersize += (8-(headersize%8));
			// Decode body if in buffer
			let body, bodysize;
			if (this._buffer.length >= headersize+header[4]) {
				let tmpbuffer = this._buffer;
				this._buffer = this._buffer.slice(headersize+header[4]);
				try {
					let signature = header[6][8];
					if (!signature) body = [];
					else {
						[body, bodysize] = unmarshal(signature, tmpbuffer.slice(headersize, headersize+header[4]), bigEndian, 0);
					}
					this._handleMessage(header, body);
			
				} catch (e) {
					console.exception("DBus: Exception while message handling:", e)
				}
			} else {
				break;
			}
		}
	
		this._asyncInput.asyncWait(this, 0, 0, gThreadManager.currentThread);
	},

	QueryInterface: function(aIID)
	{
		if (aIID.equals(Ci.nsIInputStreamCallback) || aIID.equals(Ci.nsISupports))
			return this;

		throw Cr.NS_ERROR_NO_INTERFACE;
	},
};
