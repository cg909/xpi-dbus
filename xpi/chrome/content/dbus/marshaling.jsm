
"use strict";

const EXPORTED_SYMBOLS = ["marshal", "unmarshal", "machineBigEndian", "ObjectPath", "Signature", "splitSignature"];

const Cu = Components.utils;

Cu.import("resource://gre/modules/ctypes.jsm");
const ConsoleJSM = Cu.import("resource://gre/modules/devtools/Console.jsm", {});
const console = ConsoleJSM.console; 

/**
 * @internal
 * Check if system is big endian.
 */
function isBigEndian() {
	let value = ctypes.uint32_t(1);
	let array = ctypes.cast(value, ctypes.uint8_t.array(4));
	return array[0] == 0;
}
var machineBigEndian = isBigEndian();

const BooleanTrait = {
	name: 'BOOLEAN',
	code: 'b',
	size: 4,
	align: 4,
	marshal: function (value) {
		let value = ctypes.uint32_t(value?1:0);
		return ctypes.cast(value, ctypes.uint8_t.array(4));
	},
	unmarshal: function (bytearray) {
		return ctypes.cast(bytearray, ctypes.uint32_t).value != 0;
	},
	canMarshal: function (value) { return ((typeof value) == 'boolean' || (value instanceof Boolean)); },
	consumeTypeCode: function (array) { return array.shift(); }
}

const ByteTrait = {
	name: 'BYTE',
	code: 'y',
	size: 1,
	align: 1,
	marshal: function (value) {
		if ((typeof value == 'string' || value instanceof String) && value.length == 1)
			value = ctypes.uint8_t(value.charCodeAt(0));
		if (value instanceof ctypes.char)
			value = ctypes.cast(value, ctypes.uint8_t);
		else if (!(value instanceof ctypes.uint8_t)) 
			value = ctypes.uint8_t(value);
		return ctypes.uint8_t.array(1)([value]);
	},
	unmarshal: function (bytearray) {
		return ctypes.cast(bytearray, ctypes.uint8_t).value;
	},
	canMarshal: function (value) {
		if (value instanceof ctypes.uint8_t)
			return true;
		if (value instanceof ctypes.char)
			return true;
		if ((typeof value == 'string' || value instanceof String) && value.length == 1)
			return true;
		if (typeof value == 'number' || value instanceof Number) {
			return ((value % 1 === 0) && value >= 0 && value < (1<<8));
		}
		return false;
	},
	consumeTypeCode: function (array) { return array.shift(); }
}

function IntegerTraits(name, code, bitlength, signed) {
	this.name = name;
	this.code = code;
	this.size = bitlength/8;
	this.align = this.size;
	this.consumeTypeCode = function (array) { return array.shift(); }
	
	var ctype = ctypes[(signed===true?'int':'uint')+bitlength+'_t'];
	
	this.marshal = function (value) {
		if (!(value instanceof ctype)) {
			value = ctype(value);
		}
		return ctypes.cast(value, ctypes.uint8_t.array(this.size));
	}
	
	this.unmarshal = function (bytearray, swapEndian) {
		if (swapEndian)
			bytearray = _reverseByteArray(bytearray);
		return ctypes.cast(bytearray, ctype).value;
	}
	
	if (!signed) {
		this.canMarshal = function (value) {
			if (typeof value == 'number' || value instanceof Number) {
				return ((value % 1 === 0) && value >= 0 && value < Math.pow(2, bitlength));
			}
			if (value instanceof ctype) {
				return true;
			}
			return false;
		}
	}
	else if (signed) {
		this.canMarshal = function (value) {
			if (typeof value == 'number' || value instanceof Number) {
				return ((value % 1 === 0) && value >= -(1<<(bitlength-1)) && value < (1<<(bitlength-1)));
			}
			if (value instanceof ctype) {
				return true;
			}
			return false;
		}
	}
}
const Int16Trait  = new IntegerTraits('INT16',  'n', 16, true);
const UInt16Trait = new IntegerTraits('UINT16', 'q', 16, false);
const Int32Trait  = new IntegerTraits('INT32',  'i', 32, true);
const UInt32Trait = new IntegerTraits('UINT32', 'u', 32, false);
const Int64Trait = {
	name: 'INT64',
	code: 'x',
	size: 8,
	align: 8,
	
	marshal: function (value) {
		if (!(value instanceof ctypes.int64_t))
			value = ctypes.int64_t(value);
		return ctypes.cast(value, ctypes.uint8_t.array(8));
	},
	unmarshal: function (bytearray, swapEndian) {
		if (swapEndian)
			bytearray = _reverseByteArray(bytearray);
		return ctypes.cast(bytearray, ctypes.int64_t).value;
	},
	canMarshal: function (value) {
		if (value instanceof ctypes.Int64)
			return true;
		if (value instanceof ctypes.int64_t)
			return true;
		return false;
	},
	consumeTypeCode: function (array) { return array.shift(); }
}
const UInt64Trait = {
	name: 'UINT64',
	code: 't',
	size: 8,
	align: 8,
	
	marshal: function (value) {
		if (!(value instanceof ctypes.uint64_t))
			value = ctypes.uint64_t(value);
		return ctypes.cast(value, ctypes.uint8_t.array(8));
	},
	unmarshal: function (bytearray) {
		if (swapEndian)
			bytearray = _reverseByteArray(bytearray);
		return ctypes.cast(bytearray, ctypes.uint64_t).value;
	},
	canMarshal: function (value) {
		if (value instanceof ctypes.UInt64)
			return true;
		if (value instanceof ctypes.uint64_t)
			return true;
		return false;
	},
	consumeTypeCode: function (array) { return array.shift(); }
}
const DoubleTrait = {
	name: 'DOUBLE',
	code: 'd',
	size: 8,
	align: 8,
	
	marshal: function (value) {
		if (!(value instanceof ctypes.double))
			value = ctypes.double(value);
		return ctypes.cast(value, ctypes.uint8_t.array(8));
	},
	unmarshal: function (bytearray, swapEndian) {
		if (swapEndian)
			bytearray = _reverseByteArray(bytearray);
		return ctypes.cast(bytearray, ctypes.double).value;
	},
	canMarshal: function (value) {
		if (typeof value == 'number' || value instanceof Number)
			return true;
		if (value instanceof ctypes.double)
			return true;
		return false;
	},
	consumeTypeCode: function (array) { return array.shift(); }
}
const UnixFDTrait = {
	name: 'UNIX_FD',
	code: 'h',
	size: 4,
	align: 4,
	marshal: function (value) {
		throw 'DBus: file descriptor passing is currently unsupported';
	},
	unmarshal: function (bytearray, swapEndian) {
		throw 'DBus: file descriptor passing is currently unsupported';
	},
	canMarshal: function (value) {
		return false;
	},
	consumeTypeCode: function (array) { return array.shift(); }
}

const SignatureTrait = {
	name: 'SIGNATURE',
	code: 'g',
	align: 1,
	minSize: 1,
	marshal: function (value) {
		let data = ctypes.char.array()(value.toString());
		let length = ByteTrait.marshal(data.length-1);
		return _combineMarshalled([length, ctypes.cast(data, ctypes.uint8_t.array(data.length))]);
	},
	unmarshal: function (bytearray, swapEndian, buffer, index_wrapped) {
		let length = ctypes.cast(bytearray, ctypes.uint8_t).value;
		let startindex = index_wrapped[0];
		let endindex = index_wrapped[0]+length+1;
		let stringdata = buffer.slice(startindex, endindex);
		
		index_wrapped[0] += length+1;
		return ctypes.char.array()(stringdata).readString();
	},
	canMarshal: function (value) {
		if (typeof value == 'string' || value instanceof String) {
			return value.match(/^([ybnqiuxtdhsogv]|[ma][ybnqiuxtdhsogvam(){}]+|\([ybnqiuxtdhsogvam(){}]+\))*$/) !== null;
		}
		return false;
	},
	consumeTypeCode: function (array) { return array.shift(); }
}

const StringTrait = {
	name: 'STRING',
	code: 's',
	align: 4,
	minSize: 4,
	getSize: function (value) {
		return 4+value.length+1;
	},
	marshal: function (value) {
		if (value === undefined || value === null) value = '';
		value = value.toString();
		let data = ctypes.char.array()(value);
		let length = UInt32Trait.marshal(data.length-1);
		return _combineMarshalled([length, ctypes.cast(data, ctypes.uint8_t.array(data.length))]);
	},
	unmarshal: function (bytearray, swapEndian, buffer, index_wrapped) {
		let length = UInt32Trait.unmarshal(bytearray, swapEndian);
		let stringdata = ctypes.char.array(length+1)(buffer.slice(index_wrapped[0], index_wrapped[0]+length+1));
		index_wrapped[0] += length+1;
		return stringdata.readString();
	},
	canMarshal: function (value) {
		return (typeof value == 'string' || value instanceof String || value instanceof RegExp);
	},
	consumeTypeCode: function (array) { return array.shift(); }
}
const ObjectPathTrait = {
	name: 'OBJECT_PATH',
	code: 'o',
	align: 4,
	minSize: 4,
	marshal: StringTrait.marshal,
	unmarshal: StringTrait.unmarshal,
	canMarshal: function (value) {
		if (typeof value == 'string' || value instanceof String) {
			return value.match(/^(\/|(\/[A-Za-z0-9_]+)+)$/) !== null;
		}
		return false;
	},
	consumeTypeCode: function (array) { return array.shift(); }
}

function _combineMarshalled(items) {
	if (items.length == 0) return null;
	let size = items[0].length;
	for (let i = 1; i < items.length; ++i) {
		if (items[i] === null) continue;
		size += items[i].length;
	}
	let result = ctypes.uint8_t.array(size)();
	let index = 0;
	for (let i = 0; i < items.length; ++i) {
		if (items[i] === null) continue;
		let a = items[i];
		for (let j=0; j < a.length; ++j, ++index) {
			result[index] = a[j];
		}
	}
	return result;
}

function _reverseByteArray(a) {
	let result = ctypes.uint8_t.array(a.length)();
	for (let i=0, j=a.length-1; i < a.length; i++,j--) {
		result[i] = b[j];
	}
}

const VariantTrait = {
	name: 'VARIANT',
	code: 'v',
	align: 1,
	minSize: 1,
	marshal: function (value, index) {
		let signature_str = guessValueSignature(value);
		let signature = SignatureTrait.marshal(signature_str);
		let content = marshal([value], signature_str, index+signature.length);
		return _combineMarshalled([signature, content]);
	},
	unmarshal: function (bytearray, swapEndian, buffer, index_wrapped) {
		let signature = SignatureTrait.unmarshal(bytearray, swapEndian, buffer, index_wrapped);
		let contentTrait = Traits[signature[0]];
		return _unmarshal(contentTrait, signature, buffer, swapEndian, index_wrapped);
	},
	canMarshal: function (value) {
		return (value !== null && value !== undefined);
	},
	consumeTypeCode: function (array) { return array.shift(); }
}

const ArrayTrait = {
	name: 'ARRAY',
	code: 'a',
	align: 4,
	minSize: 4,
	marshal: function (value, index, signature) {
		let signature_array = signature.split('');
		signature_array.shift();
		if (signature_array[0] == '{') {
			signature_array.shift();
			index += 4;
			let arraypadding = null;
			let outerPadding = (index % 8 != 0) ? 8-(index % 8) : 0;
			if (outerPadding) {
				arraypadding = ctypes.uint8_t.array(outerPadding)();
				index += outerPadding;
			}
			let keyTrait = Traits[signature_array[0]];
			let keySignature = keyTrait.consumeTypeCode(signature_array);
			let valueTrait = Traits[signature_array[0]];
			let valueSignature = valueTrait.consumeTypeCode(signature_array);
			let result = [];
			let count = 0;
			for (let key in value) {
				// Generate padding
				let padding = (index % 8 != 0) ? 8-(index % 8) : 0;
				if (padding) {
					index += padding;
					result.push(ctypes.uint8_t.array(padding)());
				}
				// Marshal key
				let key_data = marshal([key], keySignature, index);
				index += key_data.length;
				// Marshal value
				let value_data = marshal([value[key]], valueSignature, index);
				index += value_data.length;
				// Add to buffer
				count += 1;
				result.push(key_data, value_data)
			}
			let arraycontent = _combineMarshalled(result);
			return _combineMarshalled([UInt32Trait.marshal((arraycontent)?arraycontent.length:0), arraypadding, arraycontent]);
		}
		else {
			let count = value.length;
			let contentTrait = Traits[signature_array[0]];
			let contentSignature = contentTrait.consumeTypeCode(signature_array);
			let result = [];
			index += 4;
			let arraypadding = null;
			let outerPadding = (index % contentTrait.align != 0) ? contentTrait.align-(index % contentTrait.align) : 0;
			if (outerPadding) {
				arraypadding = ctypes.uint8_t.array(outerPadding)();
				index += outerPadding;
			}
			for (let i = 0; i < value.length; ++i) {
				let item_data = marshal([value[i]], contentSignature, index);
				index += item_data.length;
				result.push(item_data);
			}
			let arraycontent = _combineMarshalled(result);
			return _combineMarshalled([UInt32Trait.marshal((arraycontent)?arraycontent.length:0), arraypadding, arraycontent]);
		}
	},
	unmarshal: function (bytearray, swapEndian, buffer, index_wrapped, signature) {
		let signature_array = signature.split('');
		signature_array.shift();
		let arrayend = index_wrapped[0] + ctypes.cast(bytearray, ctypes.uint32_t).value;
		if (signature_array[0] == '{') {
			arrayend += (index_wrapped[0] % 8 != 0) ? 8-(index_wrapped[0] % 8) : 0;
			signature_array.shift();
			let result = {};
			let keyTrait = Traits[signature_array[0]];
			let keySignature = keyTrait.consumeTypeCode(signature_array);
			let valueTrait = Traits[signature_array[0]];
			let valueSignature = valueTrait.consumeTypeCode(signature_array);
			while (index_wrapped[0] < arrayend) {
				let padding = (index_wrapped[0] % 8 != 0) ? 8-(index_wrapped[0] % 8) : 0;
				index_wrapped[0] += padding;
				let key = _unmarshal(keyTrait, keySignature, buffer, swapEndian, index_wrapped);
				let value = _unmarshal(valueTrait, valueSignature, buffer, swapEndian, index_wrapped);
				result[key] = value;
			}
			return result;
		}
		else {
			let result = [];
			let contentTrait = Traits[signature_array[0]];
			let contentSignature = contentTrait.consumeTypeCode(signature_array);
			arrayend += (index_wrapped[0] % contentTrait.align != 0) ? contentTrait.align-(index_wrapped[0] % contentTrait.align) : 0;
			while (index_wrapped[0] < arrayend) {
				let value = _unmarshal(contentTrait, contentSignature, buffer, swapEndian, index_wrapped);
				result.push(value);
			}
			return result;
		}
		
	},
	canMarshal: function (value) {
		return (typeof value == 'object');
	},
	consumeTypeCode: function (array) {
		array.shift();
		let contentTrait = Traits[array[0]];
		return 'a' + contentTrait.consumeTypeCode(array);
	}
}

const DictEntryTrait = {
	name: 'DICT_ENTRY',
	code: '{',
	align: 8,
	minSize: 0,
	marshal: null,
	unmarshal: function () { throw "DBus: DICT_ENTRY not in ARRAY"; },
	canMarshal: function (value) {
		return false;
	},
	consumeTypeCode: function (array) {
		let result = [];
		array.shift();
		while (array[0] != '}' && array.length != 0) {
			let contentTrait = Traits[array[0]];
			result.push(contentTrait.consumeTypeCode(array));
		}
		array.shift();
		return '{' + result.join('') + '}';
	}
}
const StructTrait = {
	name: 'STRUCT',
	code: '(',
	align: 8,
	minSize: 0,
	marshal: function (value, index, signature) {
		let result = [];
		let signature_array = signature.split('');
		signature_array.shift();
		for (let i = 0; i < value.length; ++i) {
			if (signature_array[0] == ')') throw "DBus: incompatible STRUCT signature";
			let contentTrait = Traits[signature_array[0]];
			let contentSignature = contentTrait.consumeTypeCode(signature_array);
			let item_data = marshal([value[i]], contentSignature, index);
			index += item_data.length;
			result.push(item_data);
		}
		return _combineMarshalled(result);
	},
	unmarshal: function (bytearray, swapEndian, buffer, index_wrapped, signature) {
		let result = [];
		let signature_array = signature.split('');
		signature_array.shift();
		while (signature_array[0] != ')') {
			let contentTrait = Traits[signature_array[0]];
			let contentSignature = contentTrait.consumeTypeCode(signature_array);
			let value = _unmarshal(contentTrait, contentSignature, buffer, swapEndian, index_wrapped);
			result.push(value);
		}
		return result;
		
	},
	canMarshal: function (value) {
		return value instanceof Array || (value.length !== undefined);
	},
	consumeTypeCode: function (array) {
		let result = [];
		array.shift();
		while (array[0] != ')' && array.length != 0) {
			let contentTrait = Traits[array[0]];
			result.push(contentTrait.consumeTypeCode(array));
		}
		array.shift();
		return '(' + result.join('') + ')';
	}
}

function unmarshal(signature, buffer, bigendian, index) {
	if (!index) index = 0;
	var result = [];
	var index_wrapped = [index];
	var typespec = signature.split('');
	while (typespec.length > 0) {
		let trait = Traits[typespec[0]];
		let subsignature = trait.consumeTypeCode(typespec);
		result.push(_unmarshal(trait, subsignature, buffer, bigendian != machineBigEndian, index_wrapped));
	}
	return [ result, index_wrapped[0] ];
}

function _unmarshal(trait, signature, buffer, swapEndian, index_wrapped) {
	if (!trait) {
		console.error("DBus: unsupported signature: \"%s\"", signature);
		return null;
	}
	// Calculate size and alignment
	var size = (trait.size) ? trait.size : trait.minSize;
	var align = trait.align;
	// Apply alignment
	if (align > 1) {
		if (index_wrapped[0] % align != 0)
			index_wrapped[0] += (align-(index_wrapped[0]%align));
	}
	// If constant size: read the data
	var array;
	if (size) {
		let type = ctypes.uint8_t.array(size);
		let slice = buffer.slice(index_wrapped[0], index_wrapped[0]+size);
		array = ctypes.uint8_t.array(size)(slice);
		index_wrapped[0] += size;
	}
	else array = null;
	
	// Convert data
	return trait.unmarshal(array, swapEndian, buffer, index_wrapped, signature);
}

function guessValueSignature(data) {
	// ctypes
	if (data instanceof ctypes.char)
		return 'y'
	if (data instanceof ctypes.uint8_t)
		return 'y'
	if (data instanceof ctypes.int16_t)
		return 'n'
	if (data instanceof ctypes.uint16_t)
		return 'q'
	if (data instanceof ctypes.int32_t)
		return 'i'
	if (data instanceof ctypes.uint32_t)
		return 'u'
	if (data instanceof ctypes.int64_t)
		return 'x'
	if (data instanceof ctypes.uint64_t)
		return 't'
	if (data instanceof ctypes.double)
		return 'd'
	// String types
	if (data instanceof ObjectPath)
		return 'o';
	if (data instanceof Signature)
		return 'g';
	if (typeof data == 'string' || data instanceof String)
		return 's';
	// Numeric
	if (data instanceof ctypes.Int64)
		return 'x';
	if (data instanceof ctypes.UInt64)
		return 't';
	if (typeof data == 'boolean' || data instanceof Boolean)
		return 'b';
	if (typeof data == 'number' && data % 1 === 0 && data >= 0)
		return 'u';
	if (typeof data == 'number' && data % 1 === 0)
		return 'i';
	if (typeof data == 'number')
		return 'd';
	if (typeof data == 'object' && ('length' in data)) {
		return 'av'
	}
	if (typeof data == 'object') {
		return 'a{sv}'
	}
	console.error('cannot guess DBus signature',data);
	throw 'TODO: guessValueSignature';
}

function marshal(values, signature, index, padEnd) {
	if (!index) index = 0;
	if (!padEnd) padEnd = 0;
	let result = [];
	let typespec = signature.split('');
	for (let i = 0; i < values.length; ++i) {
		let trait = Traits[typespec[0]];
		if (!trait) throw 'DBus: unsupported signature';
		
		let subsignature = trait.consumeTypeCode(typespec);
		if (!trait.canMarshal(values[i])) {
			let e = new Error('DBus: cannot marshal value');
			e.value = values[i];
			e.trait = trait.name;
			e.signature = subsignature;
			console.error('DBus: cannot marshal value', values[i], trait.name, subsignature)
			throw e;
		}
		
		let align = trait.align;
		if (align > 1) {
			if (index % align != 0) {
				let padding = (align-(index%align));
				index += (align-(index%align));
				result.push(ctypes.uint8_t.array(padding)());
			}
		}
		let data = trait.marshal(values[i], index, subsignature);
		index += data.length;
		result.push(data);
	}
	
	if (padEnd > 1) {
		if (index % padEnd != 0) {
			let padding = (padEnd-(index%padEnd));
			index += (padEnd-(index%padEnd));
			result.push(ctypes.uint8_t.array(padding)());
		}
	}
	
	return _combineMarshalled(result);
}

function splitSignature(string) {
	let result = [];
	let signature = string.split('');
	while (signature.length > 0) {
		let itemTrait = Traits[signature[0]];
		let itemSignature = itemTrait.consumeTypeCode(signature);
		result.push(itemSignature);
	}
	return result;
}

const Traits = {
	y: ByteTrait,
	b: BooleanTrait,
	n: Int16Trait,
	q: UInt16Trait,
	i: Int32Trait,
	u: UInt32Trait,
	x: Int64Trait,
	t: UInt64Trait,
	d: DoubleTrait,
	h: UnixFDTrait,
	s: StringTrait,
	o: ObjectPathTrait,
	g: SignatureTrait,
	v: VariantTrait,
	a: ArrayTrait,
	'(': StructTrait,
	'{': DictEntryTrait
}

function ObjectPath(value) {
	if (!value || !ObjectPathTrait.canMarshal(value))
		throw "Not a valid ObjectPath";
	this.__value__ = value;
	Object.defineProperty(this, 'length', { value: value.length });
}
ObjectPath.prototype = Object.create(String.prototype, {
	constructor: { value: ObjectPath },
	toString: {value:function() {
		return this.__value__;
	}},
	valueOf: {value: function() {
		return this.__value__.valueOf();
	}}
});

function Signature(value) {
	if (!value) value = '';
	if (!SignatureTrait.canMarshal(value))
		throw "Not a valid Signature";
	this.__value__ = value;
	Object.defineProperty(this, 'length', { value: value.length });
}
Signature.prototype = Object.create(String.prototype, {
	constructor: { value: Signature },
	toString: {value:function() {
		return this.__value__;
	}},
	valueOf: {value: function() {
		return this.__value__.valueOf();
	}}
});
