
"use strict";

const EXPORTED_SYMBOLS = [ 'Interface', 'Method', 'Signal', 'Property', 'StandardInterfaces' ];

const Cu = Components.utils;
Cu.import("chrome://dbus-integration/content/dbus/marshaling.jsm");

function Method (name, args) {
	Object.defineProperty(this, 'name', { value: name, enumerable: true });
	Object.defineProperty(this, 'length', { value: args.length, enumerable: true });
	let in_signature = [];
	let out_signature = [];
	for (var i = 0; i < args.length; ++i) {
		Object.defineProperty(this, i, { value: args[i], enumerable: true });
		if (args[i].direction == 'in')
			in_signature.push(args[i].type);
		else if (args[i].direction == 'out')
			out_signature.push(args[i].type);
	}
	Object.defineProperty(this, 'in_signature', { value: in_signature.join(''), enumerable: true });
	Object.defineProperty(this, 'out_signature', { value: out_signature.join(''), enumerable: true });
	Object.defineProperty(this, 'annotations', { value: {}, enumerable: true });
}
Method.prototype = {name: null, length:null}

function Signal (name, args) {
	Object.defineProperty(this, 'name', { value: name, enumerable: true });
	Object.defineProperty(this, 'length', { value: args.length, enumerable: true });
	let signature = [];
	for (var i = 0; i < args.length; ++i) {
		Object.defineProperty(this, i, { value: args[i], enumerable: true });
		signature.push(args[i].type);
	}
	Object.defineProperty(this, 'signature', { value: signature.join(''), enumerable: true });
	Object.defineProperty(this, 'annotations', { value: {}, enumerable: true });
}
Signal.prototype = {name: null, length:null}

function Property (name, type, access) {
	Object.defineProperty(this, 'name', { value: name, enumerable: true });
	Object.defineProperty(this, 'type', { value: type, enumerable: true });
	Object.defineProperty(this, 'access', { value: access, enumerable: true });
	Object.defineProperty(this, 'annotations', { value: {}, enumerable: true });
}
Property.prototype = {name: null, type:null, access:null}

function Interface (name) {
	Object.defineProperty(this, 'name', { value: name, enumerable: true });
	Object.defineProperty(this, 'methods', { value: {}, enumerable: true });
	Object.defineProperty(this, 'signals', { value: {}, enumerable: true });
	Object.defineProperty(this, 'properties', { value: {}, enumerable: true });
	Object.defineProperty(this, 'annotations', { value: {}, enumerable: true });
}
Interface.prototype = {
	name: null,
	methods: null,
	signals: null,
	properties: null,
	hasProperties: false,
	annotations: null,
	addMethod: function (name, in_signature, out_signature, in_names, out_names) {
		in_signature = in_signature ? splitSignature(in_signature) : [];
		out_signature = out_signature ? splitSignature(out_signature) : [];
		var args = [];
		for (let i = 0; i < in_signature.length; ++i) {
			let sig = in_signature[i];
			let arg = { type: sig, direction: 'in' };
			if (in_names && in_names[i])
				arg.name = in_names[i];
			args.push(arg);
		}
		for (let i = 0; i < out_signature.length; ++i) {
			let sig = out_signature[i];
			let arg = { type: sig, direction: 'out' };
			if (out_names && out_names[i])
				arg.name = out_names[i];
			args.push(arg);
		}
		var result = new Method(name, args);
		this.methods[name] = result;
		return result;
	},
	addSignal: function (name, signature, names) {
		signature = signature ? splitSignature(signature) : [];
		var args = [];
		for (let i = 0; i < signature.length; ++i) {
			let sig = signature[i];
			let arg = { type: sig };
			if (names && names[i])
				arg.name = names[i];
			args.push(arg);
		}
		var result = new Signal(name, args);
		this.signals[name] = result;
		return result;
	},
	addProperty: function (name, type, access) {
		var result = new Property(name, type, access);
		this.properties[name] = result;
		this.hasProperties = true;
		return result;
	}
}

const StandardInterfaces = {}
StandardInterfaces.Introspectable = new Interface('org.freedesktop.DBus.Introspectable');
StandardInterfaces.Introspectable.addMethod('Introspect', '', 's', null, ['xml_data']);

StandardInterfaces.Properties = new Interface('org.freedesktop.DBus.Properties');
StandardInterfaces.Properties.addMethod('Get', 'ss', 'v', ['interface_name', 'property_name'], ['value']);
StandardInterfaces.Properties.addMethod('Set', 'ssv', '', ['interface_name', 'property_name', 'value']);
StandardInterfaces.Properties.addMethod('GetAll', 's', 'a{sv}', ['interface_name'], ['props']);
StandardInterfaces.Properties.addSignal('PropertiesChange', 'sa{sv}as', ['interface_name', 'changed_properties', 'invalidated_properties'])

StandardInterfaces.Peer = new Interface('org.freedesktop.DBus.Peer');
StandardInterfaces.Peer.addMethod('Ping');
StandardInterfaces.Peer.addMethod('GetMachineId', '', 's', null, ['machine_uuid']);
