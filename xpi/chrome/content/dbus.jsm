
const EXPORTED_SYMBOLS = ["DBus"];

const Cc = Components.classes;
const Ci = Components.interfaces;
const Cr = Components.results;
const Cu = Components.utils;
const CC = Components.Constructor;

const env = Cc["@mozilla.org/process/environment;1"].getService(Ci.nsIEnvironment);
const ConsoleJSM = Cu.import("resource://gre/modules/devtools/Console.jsm", {});
const console = ConsoleJSM.console; 

Cu.import("chrome://dbus-integration/content/dbus/marshaling.jsm");
Cu.import("chrome://dbus-integration/content/dbus/connection.jsm");
Cu.import("chrome://dbus-integration/content/dbus/interface.jsm");

function Bus(addr) {
	Connection.call(this, addr);
	this.names = [];
	this.callMethod(
		'/org/freedesktop/DBus',
		'Hello',
		'',
		null,
		'org.freedesktop.DBus',
		'org.freedesktop.DBus',
		function (uniqname) {
			this.unique_name = uniqname;
		}.bind(this),
		function (error) {
			console.error("DBus: org.freedesktop.DBus.Hello() threw an error: %o. This shouldn't happen on a Bus.", error);
		}.bind(this)
	);
	this._signalHandlers['org.freedesktop.DBus.NameAcquired'] = [
		{
			sender: "org.freedesktop.DBus",
			callback: function (name) { this.names.push(name); }.bind(this)
		}
	]
	this._signalHandlers['org.freedesktop.DBus.NameLost'] = [
		{
			sender: "org.freedesktop.DBus",
			callback: function (name) {
				let index = this.names.indexOf(name);
				if (index >= 0)
					this.names.splice(index, 1);
			}.bind(this)
		}
	]
}
Bus.prototype = Object.create(Connection.prototype);
Bus.prototype.constructor = Bus;
Bus.prototype.toString = function () {
	return '[dbus.Bus "'+encodeURI(this._addr)+'" "'+encodeURI(this.unique_name)+'"'+(this._transport===null?' (closed)':'')+']';
};
Bus.prototype.unique_name = null;
Bus.prototype.names = null;

Bus.prototype.close = function () {
	try {
		this.releaseAllNames();
		this._output.flush();
	} catch (e)
	{}
	Connection.prototype.close.call(this);
}

Bus.prototype.requestName = function (name, flags, callback, errorCallback) {
	if (!flags) flags = 0;
	this.callMethod(
		'/org/freedesktop/DBus',
		'RequestName',
		'su',
		[name, flags],
		'org.freedesktop.DBus',
		'org.freedesktop.DBus',
		callback,
		errorCallback
	);
}

Bus.prototype.releaseName = function (name, callback, errorCallback) {
	this.callMethod(
		'/org/freedesktop/DBus',
		'ReleaseName',
		's',
		[name],
		'org.freedesktop.DBus',
		'org.freedesktop.DBus',
		callback,
		errorCallback
	);
}

Bus.prototype.releaseAllNames = function () {
	for (let i = 0; i < this.names.length; ++i) {
		if (this.names[i][0] == ':') continue;
		this.callMethod(
			'/org/freedesktop/DBus',
			'ReleaseName',
			's',
			[this.names[i]],
			'org.freedesktop.DBus',
			'org.freedesktop.DBus'
		);
	}
}

function connectSessionBus() {
	DBus.SessionBusConnectError = null;
	var busaddr = env.get('DBUS_SESSION_BUS_ADDRESS');
	//var busaddr = 'unix:path=/tmp/dbus-test';
	if (busaddr) {
		try {
			return new Bus(busaddr);
		} catch (e) {
			DBus.SessionBusConnectError = e;
			throw e;
		}
	} else {
		DBus.SessionBusConnectError = "No Session Bus found";
		throw "No Session Bus found";
	}
}

function getSessionBus() {
	if (DBus._SessionBus === null || DBus._SessionBus._transport === null) {
		DBus._SessionBus = connectSessionBus();
	}
	return DBus._SessionBus;
}

var DBus = {
	_SessionBus: null,
	SessionBus: getSessionBus,
	Bus: Bus,
	Connection: Connection,
	Signature: Signature,
	ObjectPath: ObjectPath,
	Interface: Interface,
	Method: Method,
	Signal: Signal,
	Property: Property,
	StandardInterfaces: StandardInterfaces,
	SessionBusConnectError: null,
	
	DBUS_NAME_FLAG_ALLOW_REPLACEMENT: 0x1,
	DBUS_NAME_FLAG_REPLACE_EXISTING : 0x2,
	DBUS_NAME_FLAG_DO_NOT_QUEUE: 0x4,
	DBUS_REQUEST_NAME_REPLY_PRIMARY_OWNER: 0x1,
	DBUS_REQUEST_NAME_REPLY_IN_QUEUE: 0x2,
	DBUS_REQUEST_NAME_REPLY_EXISTS: 0x3,
	DBUS_REQUEST_NAME_REPLY_ALREADY_OWNER: 0x4,
	DBUS_RELEASE_NAME_REPLY_RELEASED: 1,
	DBUS_RELEASE_NAME_REPLY_NON_EXISTENT: 2,
	DBUS_RELEASE_NAME_REPLY_NOT_OWNER: 3
}