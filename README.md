This project aims to make Mozilla Firefox controllable via D-Bus with a D-Bus client implementation written completely in Javascript.

Inspired by [pmorch's FF-Remote-Control][].

## Current Limitations: ##

* As there is currently no support for abstract unix-domain sockets in Firefox ([Bug 979535][]), socat must be installed when connecting to abstract unix-domain sockets. As most Linux distributions use abstract domain sockets for the Session Bus, socat is in fact required on Linux setups.
* Credential passing via unix-domain sockets isn't supported yet, so the bus must accept DBUS\_COOKIE\_SHA1 or ANONYMOUS authentification methods. This excludes the System Bus in most setups.
* The Session Bus connection blocks while remote control is waiting for user authorization.
* NONCE-TCP authentication is not yet supported.
* Unix file descriptor passing is not supported.

## Features: ##

* Can provide SessionBus access to other extensions
* Windows and Tabs are exported to the SessionBus and are fully remote controllable
* Using evalByEventPassing from [pmorch's FF-Remote-Control][] you can remotely execute Javascript code in unsecured browser tabs (only HTML pages using HTTP, no HTTPS).


 [Bug 979535]: https://bugzilla.mozilla.org/show_bug.cgi?id=979535 "Bug 979535 - Cannot open unix domain sockets in abstract namespace"
 [pmorch's FF-Remote-Control]: https://github.com/pmorch/FF-Remote-Control

## API ##

### Using the API ###
To use the DBus API, you must import **chrome://dbus-integration/content/dbus.jsm**.
All functionality is capsuled in one object called **DBus**

	Components.utils.import("chrome://dbus-integration/content/dbus.jsm")
	console.log(DBus)

or (more explicit)

	var DBus = Components.utils.import("chrome://dbus-integration/content/dbus.jsm", {}).DBus
	console.log(DBus)

###	DBus.Interface ###

Instances of DBus.Interface are used to describe DBus interfaces.

#### new DBus.Interface(interface_name) ####

Creates a new interface object and returns it.

#### addMethod(name, in\_signature, out\_signature, in\_names, out\_names) ####

Add a method called *name* to the interface.

*in_signature* and *out_signature* must be valid DBus signatures or empty strings.

*in_names* and *out_names* must be null or arrays of strings. The strings are used to
name the parameters when introspection is used.

Returns a *DBus.Method* instance.

#### addSignal(name, signature, names) ####

Add a signal called *name* to the interface.

*signature* must be a valid DBus signature or an empty string.

*names* must be null or an array of strings. The strings are used for introspection.

Returns a *DBus.Signal* instance.

#### addProperty(name, type, access) ####

Add a property called *name* to the interface.

*type* must be a valid DBus type code.

*access* can be one of 'read', 'write' and 'readwrite'.

Returns a *DBus.Property* instance.

#### annotations ####

A dictionary for DBus annotations.

#### name ####

The name of the interface.
Should not be changed.

#### methods ####

Dictionary of methods.

Use `addMethod` to modify.

#### signals ####

Dictionary of signals.

Use `addSignal` to modify.

#### properties ####

Dictionary of properties.

Use `addProperty` to modify.

#### hasProperties ####

If this interface contains properties.

Automatically set by `addProperty`.

This is used to determine if the default implementation for
**org.freedesktop.DBus.Properties** should be included for objects
implementing this interface.

####Example:####

	var TestInterface = new DBus.Interface('org.example.Test');
	TestInterface.addMethod('ping', 'i', '', null, ['ping_value']);
	TestInterface.addSignal('onPing', 'i', ['ping_value']);

### DBus.Method ###

#### name ####

The name of the method.
Should not be changed.

#### length ####

Count of arguments in this method.
Should not be changed.

#### in_signature ####

Signature of 'in' paramters.

#### out_signature ####

Signature of 'out' paramters.

#### annotations ####

A dictionary for DBus annotations.

### DBus.Signal ###

#### name ####

The name of the signal.
Should not be changed.

#### length ####

Count of arguments in this signal.
Should not be changed.

#### signature ####

Signature of parameters.

#### annotations ####

A dictionary for DBus annotations.

### DBus.Property ###

#### name ####

The name of the property.
Should not be changed.

#### type ####

Type code

#### access ####

Access flag.

#### annotations ####

A dictionary for DBus annotations.

### DBus.ObjectPath ###

String-like wrapper for DBus object paths.

Essential to send object paths via variants.

### DBus.Signature ###

String-like wrapper for DBus signatures.

Essential to send signatures via variants.

### DBus.Connection ###

Instances of DBus.Connection are used for point-to-point DBus connections.

To access a Bus provided by a bus daemon, use *DBus.Bus*.

#### new DBus.Connection(addr) ####

Create a DBus connection.

*addr* must be a valid server address string.

#### close() ####

Close the connection.

The Connection object will be unusable after calling this.

#### emitSignal(path, interface, name, [signature, arguments[, destination]]) ####

Emit a signal.

#### callMethod(path, method, signature, arguments, destination, interface, [callback, [errorCallback[, noAutoStart]]]) ####

Call a method.

#### addObjectInterface(path[, interface, object]) ####

Associate a path with interface and an implementing object.

Method calls on `path` with `interface` will result in a call on a method with the same name on `object`.  
For each signal a function with the same name will be created on `object`. Calling it sends the signal.  
Properties will directly map to attribute accesses on `object`. `Object.defineProperty()` might be helpful.

If only *path* is provided, the path will only be associated with default implementations for
org.freedesktop.DBus.Peer and org.freedesktop.DBus.Introspectable.

WARNING: method signatures are not enforced. Methods may receive any arguments.

#### objectExists(path) ####

Return true if `path` is associated with any interface.

#### getObjectInterfaceImplementation(path, iface_name) ####

Return the implementation object for `path` with `iface_name`.

#### removeObjectInterface(path, iface_name) ####

Remove the association of `path` to `iface_name`.

#### removeObject(path) ####

Removes any associations for `path`, removing it from the
object tree.

### DBus.Bus ###

Inherits from `DBus.Connection`.

#### requestName(name, [flags, [callback[, errorCallback]]]) ####

Request a name on the bus.

Possible flags are:

* DBus.DBUS\_NAME\_FLAG\_ALLOW\_REPLACEMENT
* DBus.DBUS\_NAME\_FLAG\_REPLACE\_EXISTING
* DBus.DBUS\_NAME\_FLAG\_DO\_NOT\_QUEUE

`callback` will receive one of the following return values.

* DBus.DBUS\_REQUEST\_NAME\_REPLY\_PRIMARY\_OWNER
* DBus.DBUS\_REQUEST\_NAME\_REPLY\_IN\_QUEUE
* DBus.DBUS\_REQUEST\_NAME\_REPLY\_EXISTS
* DBus.DBUS\_REQUEST\_NAME\_REPLY\_ALREADY\_OWNER

#### releaseName(name[, callback[, errorCallback]]) ####

Release a name on the bus.

`callback` will receive one of the following return values.

* DBus.DBUS\_RELEASE\_NAME\_REPLY\_RELEASED
* DBus.DBUS\_RELEASE\_NAME\_REPLY\_NON\_EXISTENT
* DBus.DBUS\_RELEASE\_NAME\_REPLY\_NOT\_OWNER

#### releaseAllNames() ####

Release all names on the bus.

#### unique_name ####

Unique name of this client on the bus. Read-only.

#### names ####

Array of names this client has on the bus. Read-only.

### DBus.SessionBus() ###

Get a connection to the Session Bus.

Returns a `DBus.Bus` instance.

## Built-in DBus Interfaces ##

The module implements the standard DBus interfaces
`org.freedesktop.DBus.Peer`,
`org.freedesktop.DBus.Introspectable`,
`org.freedesktop.DBus.Properties` and
the following proprietary interfaces:

### org.mozilla.Firefox.WindowMediator ###

Implemented by the root object (/).

	method.getWindows (out ARRAY<OBJPATH> object_paths)
	method getMostRecentWindow (out OBJPATH object_path)
	signal onOpenWindow (OBJPATH object_path)

### org.mozilla.Firefox.Window ###

Implemented by all window objects (/browserwindow<var>n</var>)

	property STRING name (read-only)
	property BOOLEAN privateBrowsing (read-only)
	property UINT32 outerWidth (read-only)
	property UINT32 outerWidth (read-only)
	property UINT32 outerHeight (read-only)
	property UINT32 innerWidth (read-only)
	property UINT32 innerHeight (read-only)
	property INT32 screenX (read-only)
	property INT32 screenY (read-only)
	property BOOLEAN fullScreen (read-write)
	method resizeTo (in UINT32 width, in UINT32 height)
	method resizeBy (in INT32 widthDif, in UINT32 heightDif)
	method moveTo (in INT32 xPos, in INT32 yPos)
	method moveBy (in INT32 xPosDif, in INT32 yPosDif)
	method close ()
	method focus ()
	signal onClose ()
	signal onFullScreen (BOOLEAN)
	annotation org.freedesktop.DBus.Property.EmitsChangedSignal = false

### org.mozilla.Firefox.TabBrowser ###

Implemented by non-private window objects (/browserwindow<var>n</var>)

	method.getTabs (out ARRAY<OBJPATH> object_paths)
	method getSelectedTab (out OBJPATH object_path)
	method addTab (in STRING url, out OBJPATH object_path)
	method addTabExt(in STRING url, in DICT<STRING,VARIANT> options, out OBJPATH object_path)
	signal onTabOpen (OBJPATH object_path)
	annotation org.freedesktop.DBus.Property.EmitsChangedSignal = false

### org.mozilla.Firefox.Tab ###

Implemented by all tab objects (/browserwindow<var>n</var>/tab<var>m</var>)

	signal onTabClose ()
	signal onTabSelect ()
	signal onTabPinned ()
	signal onTabUnpinned ()
	method selectTab ()
	property STRING label (read-only)
	property BOOLEAN pinned (read-only)
	annotation org.freedesktop.DBus.Property.EmitsChangedSignal = false

### org.mozilla.Firefox.Browser ###

Implemented by all accessible tab objects (/browserwindow<var>n</var>/tab<var>m</var>).

	property STRING currentURI (read-only)
	property BOOLEAN isLoading (read-only)
	property STRING contentTitle (read-only)
	method goBack ()
	method goForward ()
	method goHome ()
	method gotoIndex (in INT32 history_index)
	method loadURI (in STRING uri)
	method reload ()
	method stop()
	method evalJavascript (in STRING code, out VARIANT result)
	method simulateClick (in UINT32 x, in UINT32 y, in BYTE button, in BOOLEAN double)
	signal onLoad (STRING uri)
	annotation org.freedesktop.DBus.Property.EmitsChangedSignal = false
